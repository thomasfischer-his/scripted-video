# SPDX-FileCopyrightText: 2022 Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: AGPL-3.0-or-later

import typing
from functools import cache
from sys import stderr
import re

import configuration


@cache
def _linesFromManuscript(manuscriptfilename: str) -> typing.List[str]:
    with open(manuscriptfilename, "r") as f:
        return [x.strip() for x in f.readlines()]


def duration(filename: str) -> typing.Optional[int]:
    starttime: typing.Optional[int] = None
    endtime: typing.Optional[int] = None
    # The following 'with' block works both for SRT and WebVTT files
    with open(filename) as subtitlefile:
        timestampstartre = re.compile(r"^((?:(?:\d+:)?\d)?\d:\d{2}[,.]\d{1,3}) -->")
        timestampendre = re.compile(r"--> ((?:(?:\d+:)?\d)?\d:\d{2}[,.]\d{1,3})$")
        for line in subtitlefile:
            line = line.strip()
            if len(line) == 0:
                # Skip empty lines, no expensive regexp'ing
                continue
            m: typing.Optional[re.Match] = None
            if starttime is None:
                m = timestampstartre.search(line)
                if m:
                    starttime = configuration.parseTimeStampToMillisecondsInt(
                        m.group(1)
                    )
            if not starttime is None:
                m = timestampendre.search(line)
                if m:
                    endtime = configuration.parseTimeStampToMillisecondsInt(m.group(1))

    if isinstance(starttime, int) and isinstance(endtime, int) and endtime > starttime:
        # Round down to previous 50ms as subtitle are timed to start 25ms later
        starttime = int(starttime // 50 * 50)
        # Round up to next 50ms as subtitle are timed to end 25ms earlier
        endtime = int((endtime + 35) // 50 * 50)
        return endtime - starttime
    else:
        return None


def _checkforlongsubtitles(
    starttime: int, endtime: int, offset: int, subtitletextlist: typing.List[str]
):
    # Time threshold to warn user if a sequence of subtitles is longer
    # than this time duration without any 'stabilizing' timestamps
    # or markers in between.
    warningtimethresholdmissingmarkers = 45000
    # Length threshold in UTF-8 characters to warn user if subtitle
    # fragment is probably too long and should be split into smaller
    # fragments using the pipe character ('|').
    warningsubtitletextlength = 110

    for subtitletext in subtitletextlist:
        if len(subtitletext) > warningsubtitletextlength:
            print(
                f"[subtitle._checkforlongsubtitles] The following subtitle fragment at or after {configuration.timeStampToStr(offset)} is quite long ({len(subtitletext)} character) and should be split into smaller fragments using the pipe symbol ('|'):",
                file=stderr,
            )
            print(subtitletext, file=stderr)
    if endtime > starttime + warningtimethresholdmissingmarkers:
        # Duration between two markers or timestamps is more that threshold
        print(
            f"[subtitle._checkforlongsubtitles] The following subtitle line at {configuration.timeStampToStr(offset)} is quite long ({configuration.timeStampToStr(endtime-starttime+500)}), consider adding time markers to 'stabilize' subtitles:",
            file=stderr,
        )
        print("|".join(subtitletextlist), file=stderr)


def computeSubtitle(
    input: configuration.Input,
    language: typing.Optional[configuration.LanguageCode],
    duration: int,
    offset: int = 0,
) -> typing.Optional[typing.List[typing.Tuple[int, typing.Optional[str]]]]:
    """For a given <input type="subtitle">, a language, and a duration, compute a list of time-text tuples.

    The last tuple contains the final time (original duration if offset==0) and None instead of a string.
    """

    if not language in input.filenames:
        return None

    linenumber: typing.Optional[int] = None
    for _attr in filter(lambda x: isinstance(x, configuration.Line), input.attributes):
        linenumber = typing.cast(configuration.Line, _attr).number
    if linenumber is None:
        return None

    marker: typing.Dict[str, int] = {}
    for _attr in filter(
        lambda x: isinstance(x, configuration.Marker), input.attributes
    ):
        attr: configuration.Marker = typing.cast(configuration.Marker, _attr)
        marker[attr.id] = attr.at - (
            offset if attr.position == configuration.Position.absolute else 0
        )
        if marker[attr.id] < 0:
            raise ValueError(
                "Time stamp in subtitle file is negative for this input: " + repr(input)
            )

    # Cache loaded lines from the subtitle file by setting a attributes to this function
    line: str = _linesFromManuscript(input.filenames[language].name)[linenumber - 1]

    p: int = -1
    currtime: int = 0
    timetotextsnipplets: dict[int, typing.List[str]] = {}
    ppipe = line.find("|", p + 1)
    while ppipe > 0 and line[ppipe - 1] == "\\":
        ppipe = line.find("|", ppipe + 1)
    pdoublecurly = line.find("{{", p + 1)
    while pdoublecurly > 0 and line[pdoublecurly - 1] == "\\":
        pdoublecurly = line.find("{{", pdoublecurly + 1)
    while ppipe > p or pdoublecurly > p:
        if ppipe > p and (ppipe < pdoublecurly or pdoublecurly < 0):
            textsnipplet = (
                line[p + 1 : ppipe].strip().replace("\\|", "|").replace("\\{{", "{{")
            )
            if len(textsnipplet) > 0:
                timetotextsnipplets.setdefault(currtime, [])
                timetotextsnipplets[currtime].append(textsnipplet)
            p = ppipe
        elif pdoublecurly > p and (pdoublecurly < ppipe or ppipe < 0):
            textsnipplet = line[p + 1 : pdoublecurly].strip()
            if len(textsnipplet) > 0:
                timetotextsnipplets.setdefault(currtime, [])
                timetotextsnipplets[currtime].append(textsnipplet)
            p = pdoublecurly
            p2 = line.find("}}", pdoublecurly + 2)
            if p2 > p:
                oldtime: int = currtime
                if line[p + 2 : p2] in marker:
                    currtime = marker[line[p + 2 : p2]]
                    if currtime >= duration:
                        raise ValueError(
                            f"Time stamp in subtitle file is outside time time range ({configuration.timeStampToStr(offset)} -- {configuration.timeStampToStr(offset+duration)}) for this input: "
                            + repr(input)
                        )
                    p = p2 + 1
                else:
                    newtime: typing.Optional[
                        int
                    ] = configuration.parseTimeStampToMillisecondsInt(line[p + 2 : p2])
                    if not newtime is None:
                        currtime = newtime - offset
                        if currtime >= duration:
                            raise ValueError(
                                f"Time stamp {configuration.timeStampToStr(newtime)} in subtitle file is outside time time range ({configuration.timeStampToStr(offset)} -- {configuration.timeStampToStr(offset+duration)}) for this input with time {configuration.timeStampToStr(currtime)}: "
                                + repr(input)
                            )
                        elif currtime < 0:
                            raise ValueError(
                                f"Time stamp {configuration.timeStampToStr(newtime)} in subtitle file is negative for this input with time {configuration.timeStampToStr(currtime)}: "
                                + repr(input)
                            )
                        p = p2 + 1
                _checkforlongsubtitles(
                    oldtime, currtime, offset, timetotextsnipplets[oldtime]
                )
        else:
            break
        ppipe = line.find("|", p + 1)
        while ppipe > 0 and line[ppipe - 1] == "\\":
            ppipe = line.find("|", ppipe + 1)
        pdoublecurly = line.find("{{", p + 1)
        while pdoublecurly > 0 and line[pdoublecurly - 1] == "\\":
            pdoublecurly = line.find("{{", pdoublecurly + 1)
    textsnipplet = line[p + 1 :].strip()
    if len(textsnipplet) > 0:
        timetotextsnipplets.setdefault(currtime, [])
        timetotextsnipplets[currtime].append(textsnipplet)
        _checkforlongsubtitles(
            currtime, duration, offset, timetotextsnipplets[currtime]
        )
    else:
        duration = currtime

    timestamps = sorted(timetotextsnipplets.keys()) + [duration]
    timetotextfragments: typing.Dict[int, typing.Optional[str]] = {}
    for start, stop in zip(timestamps[:-1], timestamps[1:]):
        alltext: str = "".join(timetotextsnipplets[start])
        time: int = start
        deltatime: int = stop - start
        for textfragment in timetotextsnipplets[start]:
            timetotextfragments[time] = textfragment
            time += int(len(textfragment) * deltatime / len(alltext) + 0.5)
    timetotextfragments[duration] = None

    return [
        (timestamp, text)
        for timestamp, text in sorted(timetotextfragments.items(), key=lambda x: x[0])
    ]


def __escape_webvtt(input: str) -> str:
    return (
        input.replace("&", "&amp;")
        .replace("<", "&lt;")
        .replace(">", "&gt;")
        .replace("{", "&#123;")
        .replace("}", "&#125;")
    )


def __escape_srt(input: str) -> str:
    # It is not ideal to replace  <  and  >  with some other, similarly looking Unicode symbols,
    # but  <  and  >  are far too likely interpreted as part of HTML tags in video viewers.
    return input.replace("<", "\u2039").replace(">", "\u203a")


def writeSubtitleFile(
    subtitles: typing.List[typing.Tuple[int, typing.Optional[str]]],
    filename: str,
    language: typing.Optional[configuration.LanguageCode] = None,
):
    format: configuration.SubtitleFormat = (
        configuration.SubtitleFormat.webvtt
        if filename.lower().endswith(".vtt")
        else configuration.SubtitleFormat.srt
    )
    millisecondseparator: str = (
        "," if format == configuration.SubtitleFormat.srt else "."
    )

    nexttimestamp: typing.Dict[int, int] = {}
    prevtimestamp: typing.Optional[int] = None
    for timestamp, _ in subtitles:
        if not prevtimestamp is None:
            nexttimestamp[prevtimestamp] = timestamp
        prevtimestamp = timestamp

    counter: int = 1
    clipmargin: int = 0
    with open(filename, "w") as subtitlefile:
        if format == configuration.SubtitleFormat.webvtt:
            # Print header for WebVTT files
            print("WEBVTT", file=subtitlefile)
            print("Kind: captions", file=subtitlefile)
            if not language is None:
                print(f"Language: {language.name}", file=subtitlefile)
            print(file=subtitlefile)

        for timestamp, text in subtitles:
            if text is None:
                continue
            if format == configuration.SubtitleFormat.srt:
                # SRT files have counters in a separate line right before time ranges
                print(counter, file=subtitlefile)
                text = __escape_srt(text)
            elif format == configuration.SubtitleFormat.webvtt:
                text = __escape_webvtt(text)
            else:
                continue
            print(
                configuration.timeStampToStr(
                    timestamp + clipmargin, sep=millisecondseparator
                ),
                "-->",
                configuration.timeStampToStr(
                    nexttimestamp[timestamp] - clipmargin, sep=millisecondseparator
                ),
                file=subtitlefile,
            )
            print(text, file=subtitlefile, end="\n\n")
            counter += 1


def writeEmptySubtitleFile(filename: str, duration: int):
    format: configuration.SubtitleFormat = (
        configuration.SubtitleFormat.webvtt
        if filename.lower().endswith(".vtt")
        else configuration.SubtitleFormat.srt
    )
    millisecondseparator: str = (
        "," if format == configuration.SubtitleFormat.srt else "."
    )

    with open(filename, "w") as subtitlefile:
        if format == configuration.SubtitleFormat.webvtt:
            # Print header for WebVTT files
            print("WEBVTT", file=subtitlefile)
            print("Kind: captions", file=subtitlefile)
            print(file=subtitlefile)

        if format == configuration.SubtitleFormat.srt:
            # SRT files have counters in a separate line right before time ranges
            print("1", file=subtitlefile)

        print(
            "00:00:00,000 -->",
            configuration.timeStampToStr(duration, sep=millisecondseparator),
            file=subtitlefile,
        )
        print(" ", file=subtitlefile, end="\n\n")
