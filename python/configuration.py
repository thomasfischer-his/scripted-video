# SPDX-FileCopyrightText: 2022 Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: AGPL-3.0-or-later

from __future__ import annotations
import xml.sax
from xml.sax.saxutils import escape, unescape
import tempfile
import typing
import os.path
import sys
import random
import subprocess
from shutil import copyfile
from enum import Enum, auto
from collections import deque
from functools import cache
import re


def tempFilename(tempDir: str, extension: str) -> str:
    if len(extension) > 1 and extension[0] == ".":
        # Remove leading dot from extension if there is any
        extension = extension[1:]
    return f"{tempDir}/{random.randint(0, 0xfffffff):07x}.{extension}"


def parseTimeStampToMillisecondsInt(input: str) -> typing.Optional[int]:
    if not isinstance(input, str):
        return None

    m = re.match(r"^\d+([.]\d+)?$", input)
    if m:
        return int(float(m.group(0)) * 1000.0 + 0.5)

    m = re.match(
        r"(?:(?:(?P<hour>\d+):)?(?P<minute>\d{1,2}):)?(?P<second>\d{1,2})(?P<fraction>[,.]\d+)?$",
        input,
    )
    if m:
        hour: int = int(m.group("hour")) if m.group("hour") else 0
        minute: int = int(m.group("minute")) if m.group("minute") else 0
        second: int = int(m.group("second")) if m.group("second") else 0
        fraction: float = (
            float("0" + m.group("fraction").replace(",", "."))
            if m.group("fraction")
            else 0.0
        )
        return ((hour * 60 + minute) * 60 + second) * 1000 + int(
            fraction * 1000.0 + 0.5
        )

    return None


def timeStampToStr(time: int, sep: str = ".") -> str:
    hour: int = int(time // 3600000)
    time -= hour * 3600000
    minute: int = int(time // 60000)
    time -= minute * 60000
    second: float = time / 1000.0
    ssecond: str = f"{second:06.3f}"
    if sep != ".":
        ssecond = ssecond.replace(".", sep)
    return f"{hour}:{minute:02d}:{ssecond}"


class Alignment(Enum):
    unset = auto()
    begin = auto()
    center = auto()
    end = auto()


class RemoveOrKeepOrForceRecode(Enum):
    unset = auto()
    remove = auto()
    keep = auto()
    forcerecode = auto()


class LanguageCode(Enum):
    en = auto()
    de = auto()
    sv = auto()

    @staticmethod
    def threeletters(code) -> typing.Optional[str]:
        # Matching language codes to 3-letter ISO 639-2 codes
        if code == LanguageCode.en:
            return "eng"
        elif code == LanguageCode.de:
            return "deu"
        elif code == LanguageCode.sv:
            return "swe"
        else:
            return None

    @staticmethod
    def englishfirstlist(
        inputset: typing.Set[LanguageCode],
    ) -> typing.List[LanguageCode]:
        if LanguageCode.en in inputset:
            inputset.remove(LanguageCode.en)
            return [LanguageCode.en] + list(inputset)
        else:
            return list(inputset)


class LanguageChoice(Enum):
    exact = auto()
    includefallback = auto()
    anything = auto()


class Quality(Enum):
    bestpossible = auto()
    high = auto()
    default = auto()
    stillacceptable = auto()
    bad = auto()
    worst = auto()


class Video:
    class Codec(Enum):
        x264 = auto()
        x265 = auto()
        av1 = auto()
        vp8 = auto()
        vp9 = auto()
        ffv1v3 = auto()
        webp = auto()
        svg = auto()
        mjpeg = auto()

        @staticmethod
        def extension(
            codec: Video.Codec, container: typing.Optional[Format.Container] = None
        ) -> str:
            if not container is None and container == Format.Container.avi:
                return ".avi"
            elif codec in [Video.Codec.vp8, Video.Codec.vp9] or (
                not container is None and container == Format.Container.webm
            ):
                return ".webm"
            elif not container is None and container == Format.Container.mp4:
                return ".mp4"
            else:
                return ".mkv"

    def __init__(self, codec: Codec, quality: typing.Optional[Quality] = None):
        self.codec: Video.Codec = codec
        self.quality: typing.Optional[Quality] = quality

    def __repr__(self):
        quality: str = (
            f' quality="{self.quality.name}"' if not self.quality is None else ""
        )
        return f'<video codec="{self.codec.name}"{quality} />'


class Audio:
    class Codec(Enum):
        opus = auto()
        ogg = auto()
        mp3 = auto()
        flac = auto()
        aac = auto()
        wav = auto()

        @staticmethod
        def fromExtension(extension: str) -> typing.Optional[Audio.Codec]:
            if extension == ".opus":
                return Audio.Codec.opus
            elif extension == ".ogg":
                return Audio.Codec.ogg
            elif extension == ".mp3":
                return Audio.Codec.mp3
            elif extension == ".flac":
                return Audio.Codec.flac
            elif extension == ".aac":
                return Audio.Codec.aac
            elif extension == ".wav":
                return Audio.Codec.wav
            else:
                return None

        @staticmethod
        def extension(codec: Audio.Codec) -> str:
            if codec == Audio.Codec.opus:
                return ".opus"
            elif codec == Audio.Codec.ogg:
                return ".ogg"
            elif codec == Audio.Codec.mp3:
                return ".mp3"
            elif codec == Audio.Codec.flac:
                return ".flac"
            elif codec == Audio.Codec.aac:
                return ".aac"
            elif codec == Audio.Codec.wav:
                return ".wav"
            else:
                return ".mkv"

    def __init__(self, codec: Codec, quality: typing.Optional[Quality] = None):
        self.codec: Audio.Codec = codec
        self.quality: typing.Optional[Quality] = quality
        # Opus is picky about sampling rates, defaults to 48000Hz; everything else is flexible
        self.samplingrate: typing.Optional[int] = (
            48000 if self.codec == Audio.Codec.opus else None
        )
        self.channels: typing.Optional[int] = None

    def __repr__(self):
        samplingrate: str = (
            f' samplingrate="{self.samplingrate}"'
            if not self.samplingrate is None
            else ""
        )
        channels: str = (
            f' channels="{self.channels}"' if not self.channels is None else ""
        )
        quality: str = (
            f' quality="{self.quality.name}"' if not self.quality is None else ""
        )
        return f'<audio codec="{self.codec.name}"{samplingrate}{channels}{quality} />'


class SubtitleFormat(Enum):
    srt = auto()
    webvtt = auto()
    default = srt

    @staticmethod
    def fromExtension(extension: str) -> typing.Optional[SubtitleFormat]:
        if extension == ".srt" or extension == "srt":
            return SubtitleFormat.srt
        elif extension == ".vtt" or extension == "vtt":
            return SubtitleFormat.webvtt
        else:
            return None

    @staticmethod
    def extension(format: SubtitleFormat) -> str:
        if format == SubtitleFormat.srt:
            return ".srt"
        elif format == SubtitleFormat.webvtt:
            return ".vtt"
        else:
            return ".mkv"

    def __repr__(self):
        return f'<subtitle format="{self.name.replace("web","")}" />'

    @staticmethod
    def matchingSubtitleFormat(
        outputformat: Format, fallbackformat: typing.Optional[SubtitleFormat]
    ) -> SubtitleFormat:
        if (
            outputformat.container == Format.Container.avi
            or outputformat.container == Format.Container.mp4
        ):
            # For use with AVI or MP4, select 'SRT'
            return SubtitleFormat.srt
        elif (
            outputformat.container == Format.Container.webm
            or outputformat.container == Format.Container.mkv
        ):
            # For use with WebM or MKV, select 'WebVTT'
            return SubtitleFormat.webvtt
        if (
            not outputformat.video is None
            and not outputformat.video.codec is None
            and outputformat.video.codec
            in [Video.Codec.av1, Video.Codec.vp8, Video.Codec.vp9]
        ):
            # VP8, VP9, and AV1 require 'WebVTT' subtitles
            return SubtitleFormat.webvtt
        elif not outputformat.subtitleformat is None:
            # If the output format specifies a specific subtitle format, use that
            return outputformat.subtitleformat
        elif not fallbackformat is None:
            return fallbackformat
        else:
            return SubtitleFormat.default


class MediaType(Enum):
    manuscript = 100
    slides = 110
    image = 120
    audio = 200
    video = 210
    subtitle = 220
    silence = 500
    placeholder = 600

    @staticmethod
    def extensions(mediatype) -> typing.Set[str]:
        if mediatype == MediaType.manuscript:
            return {".txt"}
        elif mediatype == MediaType.slides:
            return {".pdf"}
        elif mediatype == MediaType.image:
            return {".png", ".webp", ".jpg", ".jpeg", ".svg"}
        elif mediatype == MediaType.audio:
            return {".opus", ".ogg", ".oga", ".wav", ".aac", ".mp3", ".flac"}
        elif mediatype == MediaType.video:
            return {".mp4", ".webm", ".mkv", ".avi", ".mov", ".ogv"}
        elif mediatype == MediaType.subtitle:
            return {".srt", ".vtt"}
        else:
            return set()

    @staticmethod
    def extension(
        mediaType: MediaType,
        outputformat: typing.Optional[Format] = None,
        streamcount: int = 1,
    ) -> str:
        if (
            mediaType == MediaType.audio
            and not outputformat is None
            and not outputformat.audio is None
        ):
            return Audio.Codec.extension(outputformat.audio.codec)
        elif (
            mediaType == MediaType.video
            and not outputformat is None
            and not outputformat.video is None
        ):
            return Video.Codec.extension(
                outputformat.video.codec, outputformat.container
            )
        elif mediaType == MediaType.subtitle:
            if streamcount > 1:
                # Neither ".srt" or ".vtt" files can hold multiple subtitle streams, but ".mkv" can
                return ".mkv"
            elif not outputformat is None and not outputformat.subtitleformat is None:
                # Use subtitle format as specified in the output format if available
                return SubtitleFormat.extension(outputformat.subtitleformat)
            else:
                # If looking for an extension for subtitles but no specific output format
                # for subtitles was specified, fall back to the default subtitle format
                return SubtitleFormat.extension(SubtitleFormat.default)
        return ".mkv"

    @staticmethod
    def fromFilename(
        filename: str, fileprobing: bool = True
    ) -> typing.Optional[MediaType]:
        ext = filenameExtension(filename).lower()
        if fileprobing and ext == ".mkv" and os.path.exists(filename):
            from ffprobe import videoCodec, audioCodec, hasSubtitle

            ac: typing.Optional[Audio.Codec] = audioCodec(filename)[0]
            vc: typing.Optional[Video.Codec] = videoCodec(filename)[0]
            hassub: typing.Optional[bool] = hasSubtitle(filename)
            if not ac is None and vc is None and not hassub:
                return MediaType.audio
            elif not vc is None or (not ac is None and hassub):
                return MediaType.video
            elif hassub and ac is None and vc is None:
                return MediaType.subtitle
            else:
                raise ValueError(
                    f"Cannot interpret combination of {'???' if ac is None else ac.name}, {'???' if vc is None else vc}, and hassub={hassub} into MediaType"
                )

        for m in {
            MediaType.manuscript,
            MediaType.slides,
            MediaType.image,
            MediaType.audio,
            MediaType.video,
            MediaType.subtitle,
        }:
            if ext in MediaType.extensions(m):
                return m

        return None


class Position(Enum):
    absolute = auto()
    relative = auto()


class Filename:
    def __init__(
        self,
        name: str,
        lang: typing.Optional[LanguageCode] = None,
        multilang: typing.List[LanguageCode] = [],
    ):
        self.name: str = name
        self.lang: typing.Optional[LanguageCode] = (
            multilang[0] if len(multilang) > 0 and lang is None else lang
        )
        self.multilang: typing.List[LanguageCode] = (
            multilang
            if len(multilang) > 0
            else ([self.lang] if not self.lang is None else [])
        )

    def __repr__(self):
        lang = (
            f' multilang="{",".join([x.name for x in self.multilang])}"'
            if len(self.multilang) > 1
            else (f' lang="{self.lang.name}"' if not self.lang is None else "")
        )
        return f"<filename{lang}>{self.name}</filename>"

    def __lt__(self, other):
        return self.name < other.name


class Marker:
    def __init__(
        self,
        id: str,
        at: int,
        lang: LanguageCode,
        position: Position = Position.absolute,
    ):
        self.id: str = id
        self.at: int = at
        self.lang: LanguageCode = lang
        self.position: Position = position

    def __repr__(self):
        return f'<marker id="{self.id}" at="{timeStampToStr(self.at)}" position="{self.position.name}" />'


class Page:
    def __init__(self, number: int):
        self.number: int = number

    def __repr__(self):
        return f'<page number="{self.number}" />'


class Line:
    def __init__(self, number: int):
        self.number: int = number

    def __repr__(self):
        return f'<line number="{self.number}" />'


class TimeFrame:
    def __init__(
        self,
        start: int = 0,
        end: typing.Optional[int] = None,
        duration: typing.Optional[int] = None,
    ):
        self.start: int = start
        self.end: int = self.start
        self.duration: int = 0
        if isinstance(duration, int):
            self.end = start + duration
            self.duration = duration
        elif isinstance(end, int):
            self.end = end
            self.duration = end - start
        else:
            raise ValueError("Neither 'end' nor 'duration' is an int")
        if self.duration < 10:
            raise ValueError("Duration is less than 10ms")

    def __repr__(self):
        if self.start == 0:
            return f'<timeframe duration="{timeStampToStr(self.duration)}" />'
        else:
            return f'<timeframe start="{timeStampToStr(self.start)}" end="{timeStampToStr(self.end)}" />'


class Input:
    def __init__(self):
        self.ref: typing.Optional[str] = None
        self.type: typing.Optional[MediaType] = None
        self.audio: RemoveOrKeepOrForceRecode = RemoveOrKeepOrForceRecode.unset
        self.video: RemoveOrKeepOrForceRecode = RemoveOrKeepOrForceRecode.unset
        self.filenames: dict[typing.Optional[LanguageCode], Filename] = {}
        self.attributes: typing.Set[typing.Union[TimeFrame, Marker, Page, Line]] = set()
        self.alignment: Alignment = Alignment.unset

    def __repr__(self):
        ref = f' ref="{self.ref}"' if self.ref else ""
        type = f' type="{self.type.name}"' if self.type else ""
        audio = (
            f' audio="{self.audio.name}"'
            if self.audio != RemoveOrKeepOrForceRecode.unset
            else ""
        )
        video = (
            f' video="{self.video.name}"'
            if self.video != RemoveOrKeepOrForceRecode.unset
            else ""
        )
        alignment = (
            f' align="{self.alignment.name}"'
            if self.alignment != Alignment.unset
            else ""
        )
        outputblocks = [f"<input{ref}{type}{audio}{video}{alignment}>"]
        for filename in self.filenames.values():
            outputblocks.append(repr(filename))
        for attr in self.attributes:
            outputblocks.append(repr(attr))
        outputblocks.append("</input>")
        return "\n".join(outputblocks)


class Sequence:
    def __init__(self, title: typing.Optional[str] = None):
        self.inputs: typing.List[typing.Union[Input, Merge]] = []
        self.alignment: Alignment = Alignment.unset
        self.title: typing.Optional[str] = title

    def __repr__(self):
        title: str = f' title="{escape(self.title)}"' if not self.title is None else ""
        alignment = (
            f' align="{self.alignment.name}"'
            if self.alignment != Alignment.unset
            else ""
        )
        outputblocks = [f"<sequence{title}{alignment}>"]
        for input in self.inputs:
            outputblocks.append(repr(input))
        outputblocks.append("</sequence>")
        return "\n".join(outputblocks)


class Merge:
    def __init__(self, title: typing.Optional[str] = None):
        self.inputs: typing.Set[typing.Union[Input, Sequence]] = set()
        self.alignment: Alignment = Alignment.unset
        self.title: typing.Optional[str] = title

    def __repr__(self):
        title: str = f' title="{escape(self.title)}"' if not self.title is None else ""
        alignment = (
            f' align="{self.alignment.name}"'
            if self.alignment != Alignment.unset
            else ""
        )
        outputblocks = [f"<merge{title}{alignment}>"]
        for input in self.inputs:
            outputblocks.append(repr(input))
        outputblocks.append("</merge>")
        return "\n".join(outputblocks)


class Output:
    def __init__(self, type: MediaType):
        self.type: MediaType = type
        self.format: typing.Optional[Format] = None
        self.filenames: dict[typing.Optional[LanguageCode], Filename] = {}

    def __repr__(self):
        outputblocks = [f'<output type="{self.type.name}">']
        if self.format:
            outputblocks.append(repr(self.format))
        for filename in self.filenames.values():
            outputblocks.append(repr(filename))
        outputblocks.append("</output>")
        return "\n".join(outputblocks)


class Source:
    def __init__(self, id: str):
        self.id: str = id
        self.type: typing.Optional[MediaType] = None
        self.filenames: dict[typing.Optional[LanguageCode], Filename] = {}
        self.audio: RemoveOrKeepOrForceRecode = RemoveOrKeepOrForceRecode.unset
        self.video: RemoveOrKeepOrForceRecode = RemoveOrKeepOrForceRecode.unset

    def __repr__(self):
        type = f' type="{self.type.name}"' if self.type else ""
        outputblocks = [f'<source id="{self.id}"{type}>']
        for filename in self.filenames.values():
            outputblocks.append(repr(filename))
        outputblocks.append("</source>")
        return "\n".join(outputblocks)


class Size:
    def __init__(self, width: int, height: int):
        self.width: int = width
        self.height: int = height

    def __repr__(self):
        return f'<size width="{self.width}" height="{self.height}" />'


class Format:
    class Container(Enum):
        mkv = auto()
        avi = auto()
        webm = auto()
        mp4 = auto()

        @staticmethod
        def fromExtension(extension: str) -> typing.Optional[Format.Container]:
            if extension == ".mkv":
                return Format.Container.mkv
            elif extension == ".avi":
                return Format.Container.avi
            elif extension == ".webm":
                return Format.Container.webm
            elif extension == ".mp4":
                return Format.Container.mp4
            else:
                return None

        @staticmethod
        def extension(container: typing.Optional[Format.Container]) -> str:
            if container == Format.Container.mkv:
                return ".mkv"
            elif container == Format.Container.avi:
                return ".avi"
            elif container == Format.Container.webm:
                return ".webm"
            elif container == Format.Container.mp4:
                return ".mp4"
            else:
                # Fallback, even if argument 'container' is None
                return ".mkv"

    def __init__(self):
        self.audio: typing.Optional[Audio] = None
        self.video: typing.Optional[Video] = None
        self.subtitleformat: typing.Optional[SubtitleFormat] = None
        self.size: typing.Optional[Size] = None
        self.container: typing.Optional[Format.Container] = None

    def __repr__(self):
        if (
            self.audio is None
            and self.video is None
            and self.subtitleformat is None
            and self.size is None
        ):
            return ""

        container = (
            f' container="{self.container.name}"' if not self.container is None else ""
        )
        outputblocks = [f"<format{container}>"]
        if self.audio:
            outputblocks.append(repr(self.audio))
        if self.video:
            outputblocks.append(repr(self.video))
        if self.subtitleformat:
            outputblocks.append(repr(self.subtitleformat))
        if self.size:
            outputblocks.append(repr(self.size))
        if self.container:
            outputblocks.append(repr(self.container))
        outputblocks.append("</format>")
        return "\n".join(outputblocks)

    def extension(self) -> str:
        if (
            not self.subtitleformat is None
            and self.video is None
            and self.audio is None
        ):
            # Only subtitle, no video or audio
            return SubtitleFormat.extension(self.subtitleformat)
        elif not self.audio is None and self.video is None:
            # Only audio, no video
            return Audio.Codec.extension(self.audio.codec)
        elif not self.video is None:
            # Video, possibly more
            return Video.Codec.extension(self.video.codec, self.container)

        # Fallback: .mkv works always
        return ".mkv"


class Metadata:
    def __init__(self):
        self.author: typing.Dict[typing.Optional[LanguageCode], str] = {}
        self.title: typing.Dict[typing.Optional[LanguageCode], str] = {}
        self.subject: typing.Dict[typing.Optional[LanguageCode], str] = {}
        self.date: typing.Dict[typing.Optional[LanguageCode], str] = {}

    def __repr__(self):
        outputblocks = ["<metadata>"]
        for label, metadatadict in [
            ("author", self.author),
            ("title", self.title),
            ("subject", self.subject),
            ("date", self.date),
        ]:
            if metadatadict and len(metadatadict) > 0:
                for language, content in metadatadict.items():
                    languagetext: str = (
                        f' language={language.name}"'
                        if isinstance(language, LanguageCode)
                        else ""
                    )
                    outputblocks.append(f"<{label}{languagetext}>{content}</{label}>")
        outputblocks.append("</metadata>")
        return "\n".join(outputblocks)


class Movie:
    def __init__(self):
        self.metadata: typing.Optional[Metadata] = None
        self.sources: typing.Dict[str, Source] = {}
        self.outputs: typing.Set[Output] = set()
        self.concat: typing.List[Merge] = []

    def __repr__(self):
        outputblocks: typing.List[str] = ["<movie>"]
        if self.metadata:
            outputblocks.append(repr(self.metadata))
        for source in self.sources.values():
            outputblocks.append(repr(source))
        for output in self.outputs:
            outputblocks.append(repr(output))
        if len(self.concat) > 0:
            outputblocks.append("<concat>")
            for concat in self.concat:
                outputblocks.append(repr(concat))
            outputblocks.append("</concat>")
        outputblocks.append("</movie>")
        return "\n".join(outputblocks)


class ConfigurationContentHandler(xml.sax.ContentHandler):
    def __init__(
        self,
        xmlfilename: str,
        preprocessFiles: bool = False,
        tempDir: typing.Optional[str] = None,
    ):
        self.xmlfilename: str = xmlfilename
        self.preprocessFiles: bool = preprocessFiles
        self._filenamePreprocessingMapping: typing.Dict[str, str] = {}
        self.tempDir: typing.Optional[str] = tempDir
        self.tags: typing.Deque[str] = deque()
        self.movie: typing.Optional[Movie] = None
        self._global_format: typing.Optional[Format] = None
        self._newsource: typing.Optional[Source] = None
        self._newfilenamepath: typing.Optional[str] = None
        self._newfilenamelang: typing.Optional[LanguageCode] = None
        self._newfilenamemultilang: typing.List[LanguageCode] = []
        self._newoutput: typing.Optional[Output] = None
        self._newmerges: typing.List[Merge] = []
        self._newsequence: typing.Optional[Sequence] = None
        self._newinput: typing.Optional[Input] = None
        self._newstrlangtuple: typing.Optional[
            typing.Tuple[typing.Optional[str], typing.Optional[LanguageCode]]
        ] = None

    def startElement(self, tag: str, attributes: typing.Dict[str, str]):
        self.tags.append(tag)

        if tag == "movie" and len(self.tags) == 1:
            self.movie = Movie()

        elif tag == "metadata" and self.tags[-2] == "movie" and self.movie:
            self.movie.metadata = Metadata()
        elif (
            (tag == "title" or tag == "date" or tag == "author" or tag == "subject")
            and self.tags[-2] == "metadata"
            and isinstance(self.movie, Movie)
            and not self.movie.metadata is None
        ):
            self._newstrlangtuple = (
                None,
                LanguageCode[attributes["lang"]] if "lang" in attributes else None,
            )

        elif tag == "format" and self.tags[-2] == "output" and self._newoutput:
            self._newoutput.format = Format()
        elif (
            tag == "audio"
            and self.tags[-2] == "format"
            and self._newoutput
            and isinstance(self._newoutput.format, Format)
        ):
            self._newoutput.format.audio = Audio(
                Audio.Codec[attributes["codec"]],
                Quality[attributes["quality"]] if "quality" in attributes else None,
            )
            if "samplingrate" in attributes:
                self._newoutput.format.audio.samplingrate = int(
                    attributes["samplingrate"]
                )
            elif "samplerate" in attributes:
                self._newoutput.format.audio.samplingrate = int(
                    attributes["samplerate"]
                )
            if "channels" in attributes:
                self._newoutput.format.audio.channels = int(attributes["channels"])
        elif (
            tag == "video"
            and self.tags[-2] == "format"
            and self._newoutput
            and isinstance(self._newoutput.format, Format)
        ):
            self._newoutput.format.video = Video(
                Video.Codec[attributes["codec"]],
                Quality[attributes["quality"]] if "quality" in attributes else None,
            )
        elif (
            tag == "subtitle"
            and self.tags[-2] == "format"
            and self._newoutput
            and isinstance(self._newoutput.format, Format)
        ):
            self._newoutput.format.subtitleformat = SubtitleFormat.fromExtension(
                attributes["format"]
            )
        elif (
            tag == "size"
            and self.tags[-2] == "format"
            and self._newoutput
            and isinstance(self._newoutput.format, Format)
        ):
            self._newoutput.format.size = Size(
                int(attributes["width"]), int(attributes["height"])
            )

        elif tag == "format" and self.tags[-2] == "movie" and self.movie:
            self._global_format = Format()
        elif (
            tag == "audio"
            and self.tags[-2] == "format"
            and isinstance(self._global_format, Format)
        ):
            self._global_format.audio = Audio(
                Audio.Codec[attributes["codec"]],
                Quality[attributes["quality"]] if "quality" in attributes else None,
            )
            if "samplingrate" in attributes:
                self._global_format.audio.samplingrate = int(attributes["samplingrate"])
            elif "samplerate" in attributes:
                self._global_format.audio.samplingrate = int(attributes["samplerate"])
            if "channels" in attributes:
                self._global_format.audio.channels = int(attributes["channels"])
        elif (
            tag == "video"
            and self.tags[-2] == "format"
            and isinstance(self._global_format, Format)
        ):
            self._global_format.video = Video(
                Video.Codec[attributes["codec"]],
                Quality[attributes["quality"]] if "quality" in attributes else None,
            )
        elif (
            tag == "subtitle"
            and self.tags[-2] == "format"
            and isinstance(self._global_format, Format)
        ):
            self._global_format.subtitleformat = SubtitleFormat.fromExtension(
                attributes["format"]
            )
        elif (
            tag == "size"
            and self.tags[-2] == "format"
            and isinstance(self._global_format, Format)
        ):
            self._global_format.size = Size(
                int(attributes["width"]), int(attributes["height"])
            )

        elif tag == "source" and self.tags[-2] == "movie" and self.movie:
            self._newsource = Source(attributes["id"])
            if "type" in attributes:
                self._newsource.type = MediaType[attributes["type"]]
            if "audio" in attributes:
                self._newsource.audio = RemoveOrKeepOrForceRecode[attributes["audio"]]
            if "video" in attributes:
                self._newsource.video = RemoveOrKeepOrForceRecode[attributes["video"]]
            self.movie.sources[self._newsource.id] = self._newsource
        elif tag == "filename" and self._newsource:
            self._newfilenamepath = ""
            if "lang" in attributes:
                self._newfilenamelang = LanguageCode[attributes["lang"]]
            if "multilang" in attributes:
                self._newfilenamemultilang = [
                    LanguageCode[l] for l in attributes["multilang"].split(",")
                ]

        elif tag == "output" and self.tags[-2] == "movie" and self.movie:
            self._newoutput = Output(MediaType[attributes["type"]])
            self.movie.outputs.add(self._newoutput)
        elif tag == "filename" and self._newoutput:
            self._newfilenamepath = ""
            if "lang" in attributes:
                self._newfilenamelang = LanguageCode[attributes["lang"]]
            if "multilang" in attributes:
                self._newfilenamemultilang = [
                    LanguageCode[l] for l in attributes["multilang"].split(",")
                ]

        elif tag == "concat" and self.tags[-2] == "movie" and self.movie:
            self.movie.concat = []
        elif (
            tag == "merge"
            and (self.tags[-2] == "concat" or not self._newsequence is None)
            and isinstance(self.movie, Movie)
        ):
            title: typing.Optional[str] = (
                attributes["title"] if "title" in attributes else None
            )
            newmerge = Merge(title)
            if "align" in attributes:
                newmerge.alignment = Alignment[attributes["align"]]

            self._newmerges.append(newmerge)
            if self._newsequence is None:
                self.movie.concat.append(newmerge)
            else:
                self._newsequence.inputs.append(newmerge)
        elif tag == "input" and (
            (self.tags[-2] == "sequence" and self._newsequence)
            or (self.tags[-2] == "merge" and len(self._newmerges) > 0)
        ):
            self._newinput = Input()
            if "ref" in attributes:
                self._newinput.ref = attributes["ref"]
            if "type" in attributes:
                self._newinput.type = MediaType[attributes["type"]]
            if "audio" in attributes:
                self._newinput.audio = RemoveOrKeepOrForceRecode[attributes["audio"]]
            if "video" in attributes:
                self._newinput.video = RemoveOrKeepOrForceRecode[attributes["video"]]
            if self.tags[-2] == "sequence" and self._newsequence:
                self._newsequence.inputs.append(self._newinput)
            elif self.tags[-2] == "merge" and len(self._newmerges) > 0:
                self._newmerges[-1].inputs.add(self._newinput)
        elif (
            tag == "sequence" and self.tags[-2] == "merge" and len(self._newmerges) > 0
        ):
            titleattr: typing.Optional[str] = (
                attributes["title"] if "title" in attributes else None
            )
            self._newsequence = Sequence(titleattr)
            if "align" in attributes:
                self._newsequence.alignment = Alignment[attributes["align"]]
            self._newmerges[-1].inputs.add(self._newsequence)
        elif tag == "filename" and self._newinput:
            self._newfilenamepath = ""
            if "lang" in attributes:
                self._newfilenamelang = LanguageCode[attributes["lang"]]
            if "multilang" in attributes:
                self._newfilenamemultilang = [
                    LanguageCode[l] for l in attributes["multilang"].split(",")
                ]
        elif tag == "page" and self._newinput:
            self._newinput.attributes.add(Page(int(attributes["number"])))
        elif tag == "line" and self._newinput:
            self._newinput.attributes.add(Line(int(attributes["number"])))
        elif tag == "timeframe" and self._newinput:
            start: typing.Optional[int] = (
                parseTimeStampToMillisecondsInt(attributes["start"])
                if "start" in attributes
                else 0
            )
            if isinstance(start, int):
                timeframe = TimeFrame(
                    start,
                    end=parseTimeStampToMillisecondsInt(attributes["end"])
                    if "end" in attributes
                    else None,
                    duration=parseTimeStampToMillisecondsInt(attributes["duration"])
                    if "duration" in attributes
                    else None,
                )
                self._newinput.attributes.add(timeframe)

        elif tag == "marker" and self._newinput:
            at: typing.Optional[int] = parseTimeStampToMillisecondsInt(attributes["at"])
            if isinstance(at, int):
                self._newinput.attributes.add(
                    Marker(
                        attributes["id"],
                        at,
                        LanguageCode[attributes["lang"]],
                        Position[attributes["position"]],
                    )
                )

    def endElement(self, tag):
        assert tag == self.tags.pop()

        if tag == "source":
            self._newsource = None
        elif tag == "filename":
            if self._newfilenamepath is None:
                raise FileNotFoundError("No filename provided")
            elif not os.path.exists(self._newfilenamepath) and (
                not self._newsource is None or not self._newinput is None
            ):
                raise FileNotFoundError(
                    "Input file does not exist: " + self._newfilenamepath
                )

            _newfilename: Filename = Filename(
                self._newfilenamepath,
                lang=self._newfilenamelang,
                multilang=self._newfilenamemultilang,
            )
            if not self._newsource is None:
                self._newsource.filenames[_newfilename.lang] = _newfilename
            elif not self._newinput is None:
                self._newinput.filenames[_newfilename.lang] = _newfilename
            elif not self._newoutput is None:
                self._newoutput.filenames[_newfilename.lang] = _newfilename

            self._newfilenamepath = None
            self._newfilenamelang = None
            self._newfilenamemultilang = []

        elif tag == "output":
            self._newoutput = None
        elif tag == "merge":
            self._newmerges.pop()
        elif tag == "sequence":
            self._newsequence = None
        elif tag == "input":
            self._newinput = None
        elif tag == "movie" and not self.movie is None:
            # Resolve all <input ref="...">
            self._resolveInputRef(self.movie.concat)
            # Apply global <format> to individual <output> if those are missing format configuration
            self._resolveOutputFormat()
        elif (
            not self._newstrlangtuple is None
            and tag == "author"
            and self.tags[-1] == "metadata"
            and not self.movie is None
            and not self.movie.metadata is None
            and not self._newstrlangtuple[0] is None
        ):
            self.movie.metadata.author[
                self._newstrlangtuple[1]
            ] = self._newstrlangtuple[0]
        elif (
            not self._newstrlangtuple is None
            and tag == "title"
            and self.tags[-1] == "metadata"
            and not self.movie is None
            and not self.movie.metadata is None
            and not self._newstrlangtuple[0] is None
        ):
            self.movie.metadata.title[self._newstrlangtuple[1]] = self._newstrlangtuple[
                0
            ]
        elif (
            not self._newstrlangtuple is None
            and tag == "subject"
            and self.tags[-1] == "metadata"
            and not self.movie is None
            and not self.movie.metadata is None
            and not self._newstrlangtuple[0] is None
        ):
            self.movie.metadata.subject[
                self._newstrlangtuple[1]
            ] = self._newstrlangtuple[0]
        elif (
            not self._newstrlangtuple is None
            and tag == "date"
            and self.tags[-1] == "metadata"
            and not self.movie is None
            and not self.movie.metadata is None
            and not self._newstrlangtuple[0] is None
        ):
            self.movie.metadata.date[self._newstrlangtuple[1]] = self._newstrlangtuple[
                0
            ]

        self._newstrlangtuple = None

    def characters(self, content):
        if isinstance(self._newfilenamepath, str):
            self._newfilenamepath = self._preprocessFilenames(
                os.path.normpath(
                    os.path.join(os.path.dirname(self.xmlfilename), content)
                )
            )
        elif not self._newstrlangtuple is None:
            self._newstrlangtuple = (content.strip(), self._newstrlangtuple[1])

    def _preprocessFilenames(self, inputfilename: str) -> str:
        if inputfilename in self._filenamePreprocessingMapping:
            return self._filenamePreprocessingMapping[inputfilename]

        ext = filenameExtension(inputfilename)
        if (
            self.preprocessFiles
            and not self.tempDir is None
            and ext in [".ppt", ".pptx", ".odp"]
            and os.path.exists(inputfilename)
        ):
            tempfilename: str = tempFilename(self.tempDir, ext)
            copyfile(inputfilename, tempfilename)
            libreofficearguments: typing.List[str] = [
                "libreoffice",
                "--headless",
                "--convert-to",
                "pdf",
                tempfilename,
                "--outdir",
                self.tempDir,
            ]
            print(
                "[configuration._preprocessFilenames] Running:",
                "'" + "' '".join(libreofficearguments) + "'",
            )
            completedprocess: typing.Optional[
                subprocess.CompletedProcess
            ] = subprocess.run(libreofficearguments, capture_output=True)
            if completedprocess is None or completedprocess.returncode != 0:
                if not completedprocess is None:
                    print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
                    print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
                raise RuntimeError(
                    "Running command failed: '" + "' '".join(libreofficearguments) + "'"
                )

            result: str = (
                tempfilename.replace(".pptx", ".pdf")
                .replace(".ppt", ".pdf")
                .replace(".odp", ".pdf")
            )
            self._filenamePreprocessingMapping[inputfilename] = result
            return result

        return inputfilename

    def _resolveInputRef(
        self, inputs: typing.Iterable[typing.Union[Input, Merge, Sequence]]
    ):
        for input in inputs:
            if isinstance(input, Input) and not self.movie is None:
                if not input.ref is None and len(input.ref) > 0:

                    def matchingref(x: typing.Tuple[str, Source]) -> bool:
                        return isinstance(input, Input) and x[0] == input.ref

                    for _, source in filter(matchingref, self.movie.sources.items()):
                        input.type = source.type
                        input.filenames = source.filenames
                        input.ref = None
                        if input.audio == RemoveOrKeepOrForceRecode.unset:
                            input.audio = source.audio
                        if input.video == RemoveOrKeepOrForceRecode.unset:
                            input.video = source.video

                # Fix missing type for <input> by guessing type based on filenames' extension
                if input.type is None and len(input.filenames) > 0:
                    for filename in input.filenames.values():
                        ext = filenameExtension(filename.name)
                        for mediatype in {
                            MediaType.video,
                            MediaType.audio,
                            MediaType.image,
                            MediaType.subtitle,
                        }:
                            if ext in MediaType.extensions(mediatype):
                                input.type = mediatype
                                print(
                                    f"Automatically guessing type '{mediatype.name}' for input {repr(input)}"
                                )
                                break
                        if not input.type is None:
                            break

                if input.type is None:
                    raise ValueError(
                        f"Could not determine 'type' for input {repr(input)}"
                    )

            elif isinstance(input, Merge) or isinstance(input, Sequence):
                self._resolveInputRef(input.inputs)
            else:
                raise ValueError(
                    "Unknown or unsupported class to resolve input ref: "
                    + repr(type(input))
                )

    def _resolveOutputFormat(self):
        if not isinstance(self.movie, Movie):
            return

        if isinstance(self._global_format, Format):
            for output in self.movie.outputs:
                if output.format is None:
                    output.format = Format()

                if (
                    output.format.audio is None
                    and not self._global_format.audio is None
                    and output.type in [MediaType.audio, MediaType.video]
                ):
                    output.format.audio = self._global_format.audio
                if (
                    output.format.video is None
                    and not self._global_format.video is None
                    and output.type in [MediaType.video, MediaType.subtitle]
                ):
                    output.format.video = self._global_format.video
                if (
                    output.format.subtitleformat is None
                    and not self._global_format.subtitleformat is None
                    and output.type in [MediaType.video, MediaType.subtitle]
                ):
                    output.format.subtitleformat = self._global_format.subtitleformat
                if (
                    output.format.size is None
                    and not self._global_format.size is None
                    and output.type in [MediaType.video]
                ):
                    output.format.size = self._global_format.size

        # If no output formats are specified for any output,
        # make some educated guesses based on filename
        for output in self.movie.outputs:
            if len(output.filenames) < 1:
                raise ValueError("Ouput has no filenames: " + repr(output))

            extensions: typing.Set[str] = set(
                [filenameExtension(f.name.lower()) for f in output.filenames.values()]
            )
            if len(extensions) > 1:
                raise ValueError(
                    f"Mixing different extensions ('{','.join(list(extensions))}')for output not supported: "
                    + repr(output)
                )
            elif len(extensions) < 1:
                raise ValueError(
                    "Could not identify filename extensions for output: " + repr(output)
                )

            filename: str = min([f.name.lower() for f in output.filenames.values()])

            extension: str = min(extensions)
            if output.type == MediaType.subtitle and (
                output.format is None
                or not isinstance(output.format.subtitleformat, SubtitleFormat)
            ):
                if output.format is None:
                    output.format = Format()
                output.format.subtitleformat = SubtitleFormat.fromExtension(extension)
            elif output.type == MediaType.audio and (
                output.format is None or not isinstance(output.format.audio, Audio)
            ):
                if output.format is None:
                    output.format = Format()
                ac: typing.Optional[Audio.Codec] = Audio.Codec.fromExtension(extension)
                output.format.audio = Audio(ac) if not ac is None else None

            if not output.format is None:
                mt: typing.Optional[MediaType] = MediaType.fromFilename(
                    filename, fileprobing=False
                )
                if mt == MediaType.audio:
                    output.format.video = None
                elif mt == MediaType.subtitle:
                    output.format.audio = None
                    output.format.video = None


def filenameExtension(filename: str) -> str:
    """
    For a given filename, return its extension including the dot.
    If no filename extension could be detected, return the filename that was given as input.
    If argument 'filename' is not a string, this function's behaviour is undefined.
    Examples:
    - 'test.txt'  ->  '.txt'
    - 'pic.jpeg'  ->  '.jpeg'
    - 'code.c'    ->  '.c'
    - 'abcdefg'   ->  'abcdefg'
    - None        ->  ???
    - [0, 1, 2]   ->  ???
    """
    return (
        filename[-4:].lower()
        if len(filename) > 4 and filename[-4] == "."
        else (
            filename[-5:].lower()
            if len(filename) > 5 and filename[-5] == "."
            else (
                filename[-3:].lower()
                if len(filename) > 3 and filename[-3] == "."
                else (
                    filename[-2:].lower()
                    if len(filename) > 2 and filename[-2] == "."
                    else filename
                )
            )
        )
    )


def uniqueTypes(
    inputIter: typing.Iterable[typing.Union[Input, Sequence, Merge]],
    silenceToAudio=True,
) -> typing.Set[MediaType]:
    result: typing.Set[MediaType] = set()
    for input in inputIter:
        if isinstance(input, Input) and not input.type is None:
            result.add(
                MediaType.audio
                if silenceToAudio and input.type == MediaType.silence
                else input.type
            )
        elif isinstance(input, Input) and input.type is None:
            raise ValueError("Input is missing a media type")
        elif isinstance(input, Merge):
            result.update(uniqueTypes(input.inputs, silenceToAudio))
        elif isinstance(input, Sequence):
            result.update(uniqueTypes(input.inputs, silenceToAudio))
        else:
            raise ValueError("Unsupported input type: " + repr(type(input)))

    if len(result) > 1 and MediaType.placeholder in result:
        # If 'placeholder' and at least one other media type is in result,
        # it is safe to remove 'placeholder'
        result.remove(MediaType.placeholder)

    return result


def load(filename, preprocessFiles: bool = False, tempDir: typing.Optional[str] = None):
    parser = xml.sax.make_parser()
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)
    handler = ConfigurationContentHandler(filename, preprocessFiles, tempDir)
    parser.setContentHandler(handler)
    parser.parse(filename)
    return handler.movie


if __name__ == "__main__":
    if len(sys.argv) > 1:
        with tempfile.TemporaryDirectory() as tmpdirname:
            print(load(sys.argv[-1], True, tmpdirname))
