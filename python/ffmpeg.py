# SPDX-FileCopyrightText: 2022 Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: AGPL-3.0-or-later

import subprocess
import sys
import re
import os.path
import typing
from functools import cache

import configuration
import subtitle
import ffprobe
import pdftoimage

stillimageframerate: int = 8
keyint: int = 12


@cache
def metadataParameters(
    metadata: configuration.Metadata,
    language: typing.Optional[configuration.LanguageCode] = None,
) -> typing.List[str]:
    result: typing.List[str] = []
    languages: typing.List[typing.Optional[configuration.LanguageCode]] = [
        language,
        None,
        configuration.LanguageCode.en,
    ]

    gotAuthor: bool = False
    gotTitle: bool = False
    gotDate: bool = False
    for l in languages:
        if not gotAuthor and l in metadata.author:
            result.extend(["-metadata", "author=" + metadata.author[l]])
            result.extend(["-metadata", "artist=" + metadata.author[l]])
            gotAuthor = True
        if not gotTitle and l in metadata.title:
            result.extend(["-metadata", "title=" + metadata.title[l]])
            gotTitle = True
        if not gotDate and l in metadata.date:
            result.extend(["-metadata", "date=" + metadata.date[l]])
            m = re.search(r"\b(19|20)\d{2}\b", metadata.date[l])
            if not m is None:
                result.extend(["-metadata", "year=" + m.group(0)])
            gotDate = True
        if gotAuthor and gotTitle and gotDate:
            break

    return result


def _removeDuplicateFFmpegArguments(arguments: typing.List[str]) -> typing.List[str]:
    skipnext: bool = False
    seensubtitlecodec: bool = False
    result: typing.List[str] = []
    for a in arguments:
        if skipnext:
            skipnext = False
            continue
        if a == "-c:s":
            # FFmpeg complains if subtitle codec is specified multiple times,
            # which may happen if multiple subtitle streams get merged into an output video.
            # So, keep only the first subtitle codec specification.
            # All subtitle streams use the same subtitle codec specification anyway.
            if not seensubtitlecodec:
                seensubtitlecodec = True
            else:
                skipnext = True
                continue
        result.append(a)

    if arguments != result:
        print(arguments, "=>", result)
    return result


def _languageFilename(
    input: configuration.Input,
    languages: typing.Iterable[typing.Optional[configuration.LanguageCode]],
) -> typing.Optional[configuration.Filename]:
    for reflang in languages:
        for filelanguage, filename in input.filenames.items():
            if filelanguage == reflang:
                return filename

    if None in languages and len(input.filenames) > 0:
        # If no specific language is ok (as there was 'None' in 'languages')
        # and no more specific filename by language was found,
        # return an arbitrary filename
        return min(input.filenames.values())

    return None


def _generateSilenceAudiofile(
    duration: int, samplingrate: int, channels: int, tempdir: str
) -> str:
    if not isinstance(samplingrate, int):
        raise ValueError(
            f"Sampling rate must be an integer, but is {type(samplingrate)}"
        )
    if not isinstance(channels, int) and channels != 1 and channels != 2:
        raise ValueError(
            f"Number of channels must be an integer of value 1 or 2, but is {type(channels)} of value {channels}"
        )

    ffmpegarguments: typing.List[str] = [
        "ffmpeg",
        "-f",
        "lavfi",
        "-i",
        f"anullsrc=r={samplingrate}:cl={'stereo' if channels==2 else 'mono'}",
        "-to",
        configuration.timeStampToStr(duration),
        configuration.tempFilename(tempdir, "wav"),
    ]

    print(
        f"[_generateSilenceAudiofile] Creating silence of duration {duration}ms with sampling rate {samplingrate}Hz"
    )
    print(
        "[_generateSilenceAudiofile] Running:", "'" + "' '".join(ffmpegarguments) + "'"
    )
    completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
        ffmpegarguments, capture_output=True
    )
    if completedprocess is None or completedprocess.returncode != 0:
        if not completedprocess is None:
            print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
            print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
        raise RuntimeError(
            "Running command failed: '" + "' '".join(ffmpegarguments) + "'"
        )
    else:
        return ffmpegarguments[-1]


def _inputDuration(
    input: typing.Union[
        configuration.Input, configuration.Merge, configuration.Sequence
    ],
    languages: typing.List[typing.Optional[configuration.LanguageCode]],
) -> int:
    duration: int = sys.maxsize

    if isinstance(input, configuration.Input):
        # If there is any TimeFrame attribute for the input, extract its duration, else use INT_MAX
        duration = next(
            map(
                lambda x: typing.cast(configuration.TimeFrame, x).duration,
                filter(
                    lambda x: isinstance(x, configuration.TimeFrame), input.attributes
                ),
            ),
            sys.maxsize,
        )

        if duration == sys.maxsize:
            # Seemingly, no TimeFrame in input's attributes, so try to probe media file
            filename: typing.Optional[configuration.Filename] = _languageFilename(
                input, languages + [None]
            )
            if not filename is None:
                d: typing.Optional[int] = ffprobe.duration(filename.name)
                if not d is None:
                    duration = d

    elif isinstance(input, configuration.Merge):
        duration = min(
            [
                _inputDuration(m, languages)
                for m in typing.cast(configuration.Merge, input).inputs
            ]
        )
    elif isinstance(input, configuration.Sequence):
        duration = sum(
            [
                _inputDuration(s, languages)
                for s in typing.cast(configuration.Sequence, input).inputs
            ]
        )
    else:
        raise ValueError(
            "Argument to _inputDuration is neither Input, Merge, nor Sequence"
        )

    return duration


def _inputOffset(input: configuration.Input) -> int:
    # If there is any TimeFrame attribute for the input, extract its start, else use 0
    return next(
        map(
            lambda x: typing.cast(configuration.TimeFrame, x).start,
            filter(lambda x: isinstance(x, configuration.TimeFrame), input.attributes),
        ),
        0,
    )


def _constructTemporaryInput(
    filename: str, languages: typing.List[configuration.LanguageCode]
) -> typing.Tuple[configuration.Input, configuration.Filename]:
    filenameObj: configuration.Filename = configuration.Filename(
        filename, multilang=languages
    )
    inputObj: configuration.Input = configuration.Input()
    inputObj.filenames[languages[0] if len(languages) > 0 else None] = filenameObj
    inputObj.type = configuration.MediaType.fromFilename(filename)
    return inputObj, filenameObj


def _getSubtitles(
    input: configuration.Input,
    duration: int,
    offset: int = 0,
) -> typing.Dict[
    typing.Optional[configuration.LanguageCode],
    typing.List[typing.Tuple[int, typing.Optional[str]]],
]:
    subtitles: typing.Dict[
        typing.Optional[configuration.LanguageCode],
        typing.List[typing.Tuple[int, typing.Optional[str]]],
    ] = {}
    for language in typing.cast(
        typing.List[typing.Optional[configuration.LanguageCode]],
        list(configuration.LanguageCode),
    ) + [None]:
        s = subtitle.computeSubtitle(input, language, duration, offset)
        if not s is None and len(s) > 0 and not s[0][1] is None:
            subtitles[language] = s

    return subtitles


def _preprocessInput(
    input: typing.Union[
        configuration.Input, configuration.Sequence, configuration.Merge
    ],
    outputformat: configuration.Format,
    duration: int,
    tempdir: str,
    offsetforsubtitles: int = 0,
    outputlanguages: typing.List[typing.Optional[configuration.LanguageCode]] = [],
):
    if isinstance(input, configuration.Sequence):
        seq: configuration.Sequence = input
        for seqInput in seq.inputs:
            _preprocessInput(
                seqInput,
                outputformat,
                duration,
                tempdir,
                outputlanguages=outputlanguages,
            )
        return
    elif isinstance(input, configuration.Merge):
        merge: configuration.Merge = input
        for mergeInput in merge.inputs:
            _preprocessInput(
                mergeInput,
                outputformat,
                duration,
                tempdir,
                outputlanguages=outputlanguages,
            )
        return

    extension: typing.Optional[str] = (
        configuration.filenameExtension(min(input.filenames.values()).name)
        if len(input.filenames.values()) > 0
        else None
    )

    # Get target size via output->format
    size: typing.Tuple[typing.Optional[int], typing.Optional[int]] = (
        (
            outputformat.size.width,
            outputformat.size.height,
        )
        if not outputformat.size is None
        else (None, None)
    )

    newfilenames: typing.Dict[
        typing.Optional[configuration.LanguageCode], configuration.Filename
    ] = {}
    outputfilename: str = ""

    if (
        input.type == configuration.MediaType.slides
        and extension
        in configuration.MediaType.extensions(configuration.MediaType.slides)
        and isinstance(size[0], int)
        and isinstance(size[1], int)
        # Only videos have slides or images.
        and not outputformat.size is None
        and isinstance(outputformat.video, configuration.Video)
    ):
        # There is a PDF file to process, i.e. to be converted into a PNG file
        # which will be used as a still image in the video

        # Get page number to use, page number 1 is default
        pagenumber: int = next(
            map(
                lambda page: typing.cast(configuration.Page, page).number,
                filter(
                    lambda attr: isinstance(attr, configuration.Page), input.attributes
                ),
            ),
            1,
        )

        # Render PDF file's page into a WebP image stored in a temporary file
        # and return temporary file's name
        renderer = pdftoimage.PDFRenderer(typing.cast(typing.Tuple[int, int], size))
        input.type = configuration.MediaType.video
        for language, filename in input.filenames.items():
            tempfilename = configuration.tempFilename(tempdir, "webp")
            renderer.renderPDFpage(filename.name, pagenumber, tempfilename)
            newfilenames[language] = configuration.Filename(tempfilename, language)
        input.filenames = newfilenames

    elif (
        input.type == configuration.MediaType.image
        and extension
        in configuration.MediaType.extensions(configuration.MediaType.image)
        and (isinstance(outputformat.size, configuration.Size) or extension == ".svg")
        # Only videos have slides or images.
        and isinstance(outputformat.video, configuration.Video)
    ):
        input.type = configuration.MediaType.video
        for language, filename in input.filenames.items():
            _, width, height = ffprobe.videoCodec(filename.name)
            if width == None or height == None:
                del input.filenames[language]
                continue
            elif width != size[0] or height != size[1] or extension == ".svg":
                outputimagefilename = configuration.tempFilename(tempdir, "webp")
                convertarguments: typing.List[str] = [
                    "convert",
                    "-size",
                    f"{size[0]}x{size[1]}",
                    "xc:black",
                    "(",
                    filename.name,
                    "-resize",
                    f"{size[0]}x{size[1]}",
                    ")",
                    "-gravity",
                    "center",
                    "-composite",
                    "-define",
                    "webp:lossless=true",
                    outputimagefilename,
                ]
                print(
                    "[_preprocessInput] Running:",
                    "'" + "' '".join(convertarguments) + "'",
                )
                completedprocess: typing.Optional[
                    subprocess.CompletedProcess
                ] = subprocess.run(convertarguments, capture_output=True)
                if completedprocess is None or completedprocess.returncode != 0:
                    if not completedprocess is None:
                        print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
                        print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
                    raise RuntimeError(
                        "Running command failed: '" + "' '".join(convertarguments) + "'"
                    )
                    return None
                input.filenames[language] = configuration.Filename(
                    outputimagefilename, language
                )

    elif input.type == configuration.MediaType.manuscript:
        if (
            outputformat.container == configuration.Format.Container.avi
            or outputformat.container == configuration.Format.Container.mp4
            or (
                not isinstance(
                    outputformat.subtitleformat, configuration.SubtitleFormat
                )
                and not isinstance(outputformat.video, configuration.Video)
            )
        ):
            # Replace manuscript with placeholder if either
            # - AVI or MP4 container requested, but those do not support subtitles  -or-
            # - neither subtitle nor video output is requested
            input.type = configuration.MediaType.placeholder
            input.filenames = {}
            input.attributes = {
                configuration.TimeFrame(start=_inputOffset(input), duration=duration)
            }
        else:
            # Only videos or subtitles have subtitles
            subtitles = _getSubtitles(input, duration, offsetforsubtitles)
            input.type = configuration.MediaType.subtitle
            input.attributes = set()
            input.filenames = {}
            subtitleformat: configuration.SubtitleFormat = (
                configuration.SubtitleFormat.matchingSubtitleFormat(outputformat, None)
            )
            for language, subtitlelist in subtitles.items():
                subtitlefilename: str = configuration.tempFilename(
                    tempdir, configuration.SubtitleFormat.extension(subtitleformat)
                )
                subtitle.writeSubtitleFile(subtitlelist, subtitlefilename, language)
                input.filenames[language] = configuration.Filename(
                    subtitlefilename, language
                )

    elif (
        input.type == configuration.MediaType.audio
        or input.type == configuration.MediaType.silence
    ) and (
        not isinstance(outputformat, configuration.Format)
        or not isinstance(outputformat.audio, configuration.Audio)
    ):
        # If output format has no audio output defined and input may contain audio data,
        # then replace this input with a placeholder of the same length.
        audioduration: int = _inputDuration(input, list(input.filenames.keys()))
        input.type = configuration.MediaType.placeholder
        if audioduration < sys.maxsize:
            input.attributes = {
                configuration.TimeFrame(
                    start=_inputOffset(input), duration=audioduration
                )
            }
        else:
            input.attributes = set()
        input.filenames = {}

    elif input.type == configuration.MediaType.image and (
        not isinstance(outputformat, configuration.Format)
        or not isinstance(outputformat.video, configuration.Video)
    ):
        # If output format has no video output defined and input may contain image data,
        # then replace this input with a placeholder without any length specified
        input.type = configuration.MediaType.placeholder
        input.filenames = {}

    elif input.type == configuration.MediaType.video and (
        not isinstance(outputformat, configuration.Format)
        or (
            not isinstance(outputformat.video, configuration.Video)
            and not isinstance(outputformat.audio, configuration.Audio)
        )
    ):
        # If output format has no audio output and not video output defined
        # but input may contain both audio/video data,
        # then replace this input with a placeholder of the same length.
        videoduration: int = _inputDuration(input, list(input.filenames.keys()))
        input.type = configuration.MediaType.placeholder
        if videoduration < sys.maxsize:
            input.attributes = {
                configuration.TimeFrame(
                    start=_inputOffset(input), duration=videoduration
                )
            }
        else:
            input.attributes = set()
        input.filenames = {}

    elif (
        input.type == configuration.MediaType.silence
        and not outputformat.audio is None
        and not outputformat.audio.samplingrate is None
        and not outputformat.audio.channels is None
    ):
        # Input is of type 'silence', so replace it with a silent audio file
        silenceduration: int = _inputDuration(input, list(input.filenames.keys()))
        input.type = configuration.MediaType.audio
        input.attributes = set()
        input.filenames = {
            None: configuration.Filename(
                _generateSilenceAudiofile(
                    silenceduration,
                    outputformat.audio.samplingrate,
                    outputformat.audio.channels,
                    tempdir,
                ),
                None,
            )
        }

    elif (
        input.type == configuration.MediaType.video
        and input.video == configuration.RemoveOrKeepOrForceRecode.remove
    ):
        # If from a video file only audio tracks are to be used
        filename = min(input.filenames.values())
        audiocodec, _, _ = ffprobe.audioCodec(filename.name)
        outputextension: str = (
            configuration.Audio.Codec.extension(audiocodec)
            if not audiocodec is None
            else ".mkv"
        )

        for filename in input.filenames.values():
            languages: typing.List[typing.Optional[configuration.LanguageCode]] = [
                l for l in filename.multilang
            ]
            if len(languages) == 0:
                languages = [None]

            for lang in languages:
                outputfilename = configuration.tempFilename(tempdir, outputextension)
                ffmpeglang = (
                    "0"
                    if lang is None
                    else f"language:{configuration.LanguageCode.threeletters(lang)}"
                )
                ffmpegarguments = [
                    "ffmpeg",
                    "-i",
                    filename.name,
                    "-map",
                    f"0:a:{ffmpeglang}",
                    "-c:a",
                    "copy",
                    outputfilename,
                ]
                print(
                    "[_preprocessInput] Running:",
                    "'" + "' '".join(ffmpegarguments) + "'",
                )
                completedprocess = subprocess.run(ffmpegarguments, capture_output=True)
                if completedprocess is None or completedprocess.returncode != 0:
                    if not completedprocess is None:
                        print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
                        print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
                    raise RuntimeError(
                        "Running command failed: '" + "' '".join(ffmpegarguments) + "'"
                    )
                    return None
                else:
                    newfilenames[lang] = configuration.Filename(
                        outputfilename, lang=lang
                    )

        input.filenames = newfilenames
        input.type = configuration.MediaType.audio
        input.video = configuration.RemoveOrKeepOrForceRecode.unset

    # If there are several input files for one input that just differ in language
    # and more than one of those languages are requested, then merge those input files
    # into one .mkv file, assuming multiple such inputs may co-exist in the same output file,
    # for example multiple audio or subtitle streams
    if (
        input.type == configuration.MediaType.subtitle
        or input.type == configuration.MediaType.audio
    ):
        mediaToMerge: typing.Dict[
            configuration.LanguageCode, configuration.Filename
        ] = {}
        for lng, fn in filter(
            lambda l: l[0] in outputlanguages, input.filenames.items()
        ):
            if not lng is None:
                mediaToMerge[lng] = fn
        if len(mediaToMerge) > 1:
            outputfilename = configuration.tempFilename(tempdir, "mkv")
            mediatypeletter: str = (
                "a"
                if input.type == configuration.MediaType.audio
                else ("s" if input.type == configuration.MediaType.subtitle else "x")
            )
            mediacounter: int = 0
            ffmpegarguments = ["ffmpeg"]
            i: typing.List[str] = []
            o: typing.List[str] = []
            for lng, fn in mediaToMerge.items():
                i.extend(["-i", fn.name])
                o.extend(
                    [
                        "-map",
                        f"{mediacounter}:{mediatypeletter}",
                        f"-metadata:s:{mediatypeletter}:{mediacounter}",
                        f"language={configuration.LanguageCode.threeletters(lng)}",
                    ]
                )
                # ffmpegarguments.extend(
                #    ["-i", fn.name, f"-metadata:s:{mediatypeletter}:{mediacounter}", f"language={configuration.LanguageCode.threeletters(lng)}", f"-c:{mediatypeletter}", "copy"])
                mediacounter += 1
            ffmpegarguments.extend(i)
            ffmpegarguments.extend(o)
            ffmpegarguments.extend([f"-c:{mediatypeletter}", "copy", outputfilename])
            print(
                "[_preprocessInput] Running:", "'" + "' '".join(ffmpegarguments) + "'"
            )
            completedprocess = subprocess.run(ffmpegarguments, capture_output=True)
            if (
                completedprocess is None
                or completedprocess.returncode != 0
                and not b"Output file is empty" in completedprocess.stderr
            ):
                if not completedprocess is None:
                    print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
                    print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
                raise RuntimeError(
                    "Running command failed: '" + "' '".join(ffmpegarguments) + "'"
                )
                return None
            input.filenames = {
                None: configuration.Filename(
                    outputfilename, multilang=list(mediaToMerge.keys())
                )
            }


def _audioQualityToFFmpegArguments(
    audio: typing.Optional[configuration.Audio],
    codec: configuration.Audio.Codec,
    overrideQuality: typing.Optional[configuration.Quality] = None,
) -> typing.List[str]:
    if audio is None:
        return []
    quality: typing.Optional[configuration.Quality] = (
        audio.quality if overrideQuality is None else overrideQuality
    )
    if codec == configuration.Audio.Codec.mp3:
        qualityToQuality = {
            None: "4",
            configuration.Quality.bestpossible: "0",
            configuration.Quality.high: "2",
            configuration.Quality.default: "4",
            configuration.Quality.stillacceptable: "6",
            configuration.Quality.bad: "8",
            configuration.Quality.worst: "9",
        }
        if quality in qualityToQuality:
            return ["-qscale:a", qualityToQuality[quality]]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for MP3 in _audioQualityToFFmpegArguments"
            )
    elif codec == configuration.Audio.Codec.ogg:
        qualityToFloat = {
            None: 6.0,
            configuration.Quality.bestpossible: 10.0,
            configuration.Quality.high: 8.0,
            configuration.Quality.default: 6.0,
            configuration.Quality.stillacceptable: 4.0,
            configuration.Quality.bad: 1.5,
            configuration.Quality.worst: -1.0,
        }
        if quality in qualityToFloat:
            return ["-qscale:a", f"{qualityToFloat[quality]:0.1f}"]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for OGG in _audioQualityToFFmpegArguments"
            )
    elif codec == configuration.Audio.Codec.opus:
        qualityToBitrate = {
            None: 32,
            configuration.Quality.bestpossible: 128,
            configuration.Quality.high: 64,
            configuration.Quality.default: 32,
            configuration.Quality.stillacceptable: 24,
            configuration.Quality.bad: 16,
            configuration.Quality.worst: 8,
        }
        if quality in qualityToBitrate:
            return ["-b:a", f"{qualityToBitrate[quality]}k"]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for Opus in _audioQualityToFFmpegArguments"
            )
    elif codec == configuration.Audio.Codec.aac:
        qualityToBitrate = {
            None: 96,
            configuration.Quality.bestpossible: 192,
            configuration.Quality.high: 128,
            configuration.Quality.default: 96,
            configuration.Quality.stillacceptable: 64,
            configuration.Quality.bad: 32,
            configuration.Quality.worst: 16,
        }
        if quality in qualityToBitrate:
            return ["-b:a", f"{qualityToBitrate[quality]}k"]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for AAC in _audioQualityToFFmpegArguments"
            )
    return []


def _videoCodecToFFmpegArguments(
    video: typing.Optional[configuration.Video],
    size: typing.Tuple[typing.Optional[int], typing.Optional[int]],
    overrideQuality: typing.Optional[configuration.Quality] = None,
) -> typing.List[str]:
    if video is None:
        return []
    quality: typing.Optional[configuration.Quality] = (
        video.quality if overrideQuality is None else overrideQuality
    )
    if video.codec == configuration.Video.Codec.vp8:
        qualityToCRF = {
            None: 10,
            configuration.Quality.bestpossible: 0,
            configuration.Quality.high: 5,
            configuration.Quality.default: 10,
            configuration.Quality.stillacceptable: 16,
            configuration.Quality.bad: 31,
            configuration.Quality.worst: 63,
        }
        if quality in qualityToCRF:
            return [
                "-c:v",
                "libvpx",
                "-qmin",
                "0",
                "-qmax",
                str(min(63, max(qualityToCRF[quality] + 2, 50))),
                "-crf",
                str(qualityToCRF[quality]),
            ]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for VP8 in _videoCodecToFFmpegArguments"
            )
    elif video.codec == configuration.Video.Codec.vp9:
        qualityToCRF = {
            None: 17,
            configuration.Quality.bestpossible: 0,
            configuration.Quality.high: 9,
            configuration.Quality.default: 17,
            configuration.Quality.stillacceptable: 28,
            configuration.Quality.bad: 44,
            configuration.Quality.worst: 63,
        }
        if quality in qualityToCRF:
            return [
                "-c:v",
                "libvpx-vp9",
                "-qmin",
                "0",
                "-qmax",
                str(min(63, max(qualityToCRF[quality] + 2, 50))),
                "-crf",
                str(qualityToCRF[quality]),
                "-b:v",
                "0",
            ]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for VP9 in _videoCodecToFFmpegArguments"
            )
    elif video.codec == configuration.Video.Codec.x264:
        qualityToCRF = {
            None: 22,
            configuration.Quality.bestpossible: 10,
            configuration.Quality.high: 16,
            configuration.Quality.default: 22,
            configuration.Quality.stillacceptable: 29,
            configuration.Quality.bad: 39,
            configuration.Quality.worst: 51,
        }
        if quality in qualityToCRF:
            return [
                "-c:v",
                "libx264",
                "-crf",
                str(qualityToCRF[quality]),
                "-tune",
                "stillimage",
            ]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for x264 in _videoCodecToFFmpegArguments"
            )
    elif video.codec == configuration.Video.Codec.x265:
        qualityToCRF = {
            None: 27,
            configuration.Quality.bestpossible: 13,
            configuration.Quality.high: 20,
            configuration.Quality.default: 27,
            configuration.Quality.stillacceptable: 33,
            configuration.Quality.bad: 40,
            configuration.Quality.worst: 51,
        }
        if quality in qualityToCRF:
            return ["-c:v", "libx265", "-crf", str(qualityToCRF[quality])]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for x265 in _videoCodecToFFmpegArguments"
            )
    elif video.codec == configuration.Video.Codec.av1:
        qualityToCRF = {
            None: 17,
            configuration.Quality.bestpossible: 4,
            configuration.Quality.high: 9,
            configuration.Quality.default: 17,
            configuration.Quality.stillacceptable: 28,
            configuration.Quality.bad: 44,
            configuration.Quality.worst: 63,
        }
        if quality in qualityToCRF:
            return [
                "-c:v",
                "libaom-av1",
                "-crf",
                str(qualityToCRF[quality]),
                "-b:v",
                "0",
            ]
        else:
            raise ValueError(
                f"Quality {'None' if quality is None else quality.name} not know for AV1 in _videoCodecToFFmpegArguments"
            )
    elif video.codec == configuration.Video.Codec.ffv1v3:
        return ["-c:v", "ffv1", "-level", "3"]
    return []


def _codecArgumentsForFile(
    filename: configuration.Filename,
    input: configuration.Input,
    outputformat: configuration.Format,
    inputindex: int,
    duration: int = sys.maxsize,
    skipAudio: bool = False,
) -> typing.Tuple[list[str], list[str], typing.Optional[str]]:
    inputextension: str = configuration.filenameExtension(filename.name)
    # By default, output extension is input extension, e.g. '.mkv' or '.srt'
    outputextension: str = inputextension
    inputaudiocodec, inputsamplingrate, inputaudiochannels = ffprobe.audioCodec(
        filename.name
    )
    inputvideocodec, inputwidth, inputheight = ffprobe.videoCodec(filename.name)

    inputarguments = []
    outputarguments = []

    targetaudiocodec: typing.Optional[configuration.Audio.Codec] = (
        outputformat.audio.codec
        if isinstance(outputformat.audio, configuration.Audio)
        and isinstance(outputformat.audio.codec, configuration.Audio.Codec)
        and inputaudiocodec
        else None
    )
    targetsamplingrate: typing.Optional[int] = (
        outputformat.audio.samplingrate
        if isinstance(outputformat.audio, configuration.Audio)
        and isinstance(outputformat.audio.samplingrate, int)
        else 48000
    )
    targetaudiochannels: typing.Optional[int] = (
        outputformat.audio.channels
        if isinstance(outputformat.audio, configuration.Audio)
        and isinstance(outputformat.audio.channels, int)
        else 1
    )
    targetvideocodec: typing.Optional[configuration.Video.Codec] = (
        outputformat.video.codec
        if isinstance(outputformat.video, configuration.Video)
        and isinstance(outputformat.video.codec, configuration.Video.Codec)
        and inputvideocodec
        else None
    )
    targetcontainer: typing.Optional[configuration.Format.Container] = (
        outputformat.container
        if isinstance(outputformat.container, configuration.Format.Container)
        else None
    )

    size: typing.Tuple[typing.Optional[int], typing.Optional[int]] = (
        (outputformat.size.width, outputformat.size.height)
        if isinstance(outputformat.size, configuration.Size)
        else (None, None)
    )

    opusoutputarguments = (
        [
            "-c:a",
            "libopus",
            *_audioQualityToFFmpegArguments(
                outputformat.audio, configuration.Audio.Codec.opus
            ),
            "-vbr",
            "on",
            "-compression_level",
            "6",
            "-application",
            "audio",
            "-frame_duration",
            "60",
            "-ar",
            str(targetsamplingrate),
            "-ac",
            str(targetaudiochannels),
        ]
        if targetaudiocodec == configuration.Audio.Codec.opus
        else []
    )
    oggoutputarguments = (
        [
            "-c:a",
            "libvorbis",
            *_audioQualityToFFmpegArguments(
                outputformat.audio, configuration.Audio.Codec.ogg
            ),
            "-ar",
            str(targetsamplingrate),
            "-ac",
            str(targetaudiochannels),
        ]
        if targetaudiocodec == configuration.Audio.Codec.ogg
        else []
    )
    mp3outputarguments = (
        [
            "-c:a",
            "libmp3lame",
            *_audioQualityToFFmpegArguments(
                outputformat.audio, configuration.Audio.Codec.mp3
            ),
            "-ar",
            str(targetsamplingrate),
            "-ac",
            str(targetaudiochannels),
        ]
        if targetaudiocodec == configuration.Audio.Codec.mp3
        else []
    )
    aacoutputarguments = (
        [
            "-c:a",
            "aac",
            *_audioQualityToFFmpegArguments(
                outputformat.audio, configuration.Audio.Codec.aac
            ),
            "-ar",
            str(targetsamplingrate),
            "-ac",
            str(targetaudiochannels),
        ]
        if targetaudiocodec == configuration.Audio.Codec.aac
        else []
    )

    # Determine clipping ranges if configured via duration or time frame
    timeframe: typing.Optional[configuration.TimeFrame] = next(
        filter(lambda x: isinstance(x, configuration.TimeFrame), input.attributes), None
    )
    start: int = 0
    end: int = duration
    if not timeframe is None and not timeframe.start is None:
        start = timeframe.start
        end = (
            timeframe.end
            if not timeframe.end is None
            else (start + timeframe.duration if not timeframe.duration is None else 1)
        )
        duration = min(duration, end - start)
    elif os.path.exists(filename.name):
        _end: typing.Optional[int] = ffprobe.duration(filename.name)
        end = duration if _end is None else _end

    if duration < sys.maxsize and input.alignment != configuration.Alignment.unset:
        if input.alignment == configuration.Alignment.begin:
            end = start + duration
        elif input.alignment == configuration.Alignment.center:
            center = int((end + start) // 2)
            start = int(center - duration // 2)
            end = int(center + duration // 2)
        elif input.alignment == configuration.Alignment.end:
            start = end - duration

    if start > 0:
        inputarguments.extend(["-ss", configuration.timeStampToStr(start)])
    if end < sys.maxsize:
        inputarguments.extend(
            [
                "-to",
                configuration.timeStampToStr(end),
            ]
        )

    ### SUBTITLES ###
    if inputextension in configuration.MediaType.extensions(
        configuration.MediaType.subtitle
    ):
        subtitleformat: typing.Dict[str, str] = {".srt": "srt", ".vtt": "webvtt"}
        inputarguments.extend(
            [
                "-sub_charenc",
                "UTF-8",
                "-f",
                subtitleformat[inputextension],
                "-i",
                filename.name,
            ]
        )
        # By default, subtitle output format is the same as the input format
        subtitleoutputformat: configuration.SubtitleFormat = (
            configuration.SubtitleFormat.matchingSubtitleFormat(
                outputformat, configuration.SubtitleFormat.fromExtension(inputextension)
            )
        )
        ffmpegsubtitleformat: typing.Dict[configuration.SubtitleFormat, str] = {
            configuration.SubtitleFormat.srt: "srt",
            configuration.SubtitleFormat.webvtt: "webvtt",
        }
        outputextension = configuration.SubtitleFormat.extension(subtitleoutputformat)
        outputarguments.extend(
            [
                "-map",
                str(inputindex),
                "-c:s",
                ffmpegsubtitleformat[subtitleoutputformat],
            ]
        )

    elif (
        inputextension == ".mkv"
        and inputaudiocodec is None
        and inputvideocodec is None
        and ffprobe.hasSubtitle(filename.name)
    ):
        # Multiple subtitles in one Matroska container
        inputarguments.extend(
            [
                # No special formatting, assuming that was done when creeating the container
                "-i",
                filename.name,
            ]
        )
        outputarguments.extend(["-map", str(inputindex), "-c:s", "copy"])
        outputextension = ".mkv"  # same as inputextension

    ### AUDIO ENCODING ###
    elif skipAudio and inputextension in configuration.MediaType.extensions(
        configuration.MediaType.audio
    ):
        # The function argument skipAudio states that no audio is to be processed
        # but the input is just an audio file, so return empty input and output arguments
        # to not process this input at all
        return ([], [], None)
    elif (
        targetsamplingrate == inputsamplingrate
        and targetaudiocodec == inputaudiocodec
        and targetaudiochannels == inputaudiochannels
        and (
            inputextension != ".mkv"
            or (inputvideocodec, inputwidth, inputheight) == (None, None, None)
        )
    ):
        # Input sampling rate and target sampling rate are a match --and--
        # input audio codec and target audio codec are a match --and--
        # input is not a .mkv file or if it is one, it does not contain any video
        inputarguments.extend(["-i", filename.name])
        outputarguments.extend(["-map", str(inputindex), "-c:a", "copy"])
        if ffprobe.hasSubtitle(filename.name):
            outputarguments.extend(["-c:s", "copy"])
        outputarguments.extend(["-avoid_negative_ts", "make_zero"])
    elif inputextension in configuration.MediaType.extensions(
        configuration.MediaType.audio
    ) or (
        inputextension == ".mkv"
        and (inputvideocodec, inputwidth, inputheight) == (None, None, None)
        and not inputaudiocodec is None
        and not inputaudiochannels is None
    ):
        # Input file is either audio or a .mkv file without video,
        # but the audio parameters (codec or sampling) do not match,
        # so recoding is necessary
        if targetaudiocodec == configuration.Audio.Codec.ogg:
            inputarguments.extend(["-i", filename.name])
            outputarguments.extend(["-map", str(inputindex)])
            outputarguments.extend(oggoutputarguments)
            outputextension = configuration.Audio.Codec.extension(targetaudiocodec)
        elif (
            targetaudiocodec == configuration.Audio.Codec.opus
            and not opusoutputarguments is None
        ):
            inputarguments.extend(["-i", filename.name])
            outputarguments.extend(["-map", str(inputindex)])
            outputarguments.extend(opusoutputarguments)
            outputextension = configuration.Audio.Codec.extension(targetaudiocodec)
        elif (
            targetaudiocodec == configuration.Audio.Codec.mp3
            and not mp3outputarguments is None
        ):
            inputarguments.extend(["-i", filename.name])
            outputarguments.extend(["-map", str(inputindex)])
            outputarguments.extend(mp3outputarguments)
            outputextension = configuration.Audio.Codec.extension(targetaudiocodec)
        elif (
            targetaudiocodec == configuration.Audio.Codec.aac
            and not aacoutputarguments is None
        ):
            inputarguments.extend(["-i", filename.name])
            outputarguments.extend(["-map", str(inputindex)])
            outputarguments.extend(aacoutputarguments)
            outputextension = configuration.Audio.Codec.extension(targetaudiocodec)
        else:  # unsupported or no target audio codec given
            return ([], [], None)

    ### STILL IMAGE ENCODING ###
    elif inputextension in configuration.MediaType.extensions(
        configuration.MediaType.image
    ):
        inputarguments.extend(
            [
                "-loop",
                "1",
                "-r",
                str(stillimageframerate),
                "-i",
                filename.name,
            ]
        )
        outputarguments.extend(["-map", str(inputindex)])
        if targetvideocodec == configuration.Video.Codec.x264:
            outputarguments.extend(
                _videoCodecToFFmpegArguments(
                    outputformat.video, size, configuration.Quality.bestpossible
                )
            )
            outputarguments.extend(["-x264opts", "keyint=" + str(keyint)])
        elif targetvideocodec == configuration.Video.Codec.x265:
            outputarguments.extend(
                _videoCodecToFFmpegArguments(
                    outputformat.video, size, configuration.Quality.bestpossible
                )
            )
            outputarguments.extend(
                ["-x265-params", "lossless=1:preset=medium:keyint=" + str(keyint)]
            )
        elif targetvideocodec == configuration.Video.Codec.av1:
            outputarguments.extend(
                _videoCodecToFFmpegArguments(
                    outputformat.video, size, configuration.Quality.bestpossible
                )
            )
            outputarguments.extend(
                [
                    "-g",
                    str(keyint),
                    "-keyint_min",
                    str(keyint),
                    "-aom-params",
                    "lossless=1",
                ]
            )
        elif targetvideocodec == configuration.Video.Codec.vp8:
            outputarguments.extend(
                _videoCodecToFFmpegArguments(
                    outputformat.video, size, configuration.Quality.bestpossible
                )
            )
            outputarguments.extend(["-g", str(keyint), "-keyint_min", str(keyint)])
        elif targetvideocodec == configuration.Video.Codec.vp9:
            outputarguments.extend(
                _videoCodecToFFmpegArguments(
                    outputformat.video, size, configuration.Quality.bestpossible
                )
            )
            outputarguments.extend(
                ["-g", str(keyint), "-keyint_min", str(keyint), "-lossless", "1"]
            )
        elif targetvideocodec == configuration.Video.Codec.ffv1v3:
            outputarguments.extend(
                _videoCodecToFFmpegArguments(
                    outputformat.video, size, configuration.Quality.bestpossible
                )
            )
        else:  # unsupported or no target video codec given
            return ([], [], None)

        outputextension = configuration.Format.Container.extension(targetcontainer)

    ### VIDEO ENCODING ###
    elif (
        inputextension
        in configuration.MediaType.extensions(configuration.MediaType.video)
        and not inputvideocodec is None
        and not size == (None, None)
    ):
        outputextension = configuration.Format.Container.extension(targetcontainer)
        if inputvideocodec == configuration.Video.Codec.x264:
            # Time stamp issue with h264 must be addressed, see also here:
            #   https://video.stackexchange.com/questions/24462/timestamps-are-unset-in-a-packet-for-stream-0-this-is-deprecated-and-will-stop
            inputarguments.extend(["-use_wallclock_as_timestamps", "1"])
        inputarguments.extend(["-i", filename.name])
        outputarguments.extend(["-map", str(inputindex)])
        if skipAudio or input.audio == configuration.RemoveOrKeepOrForceRecode.remove:
            # Do not keep audio if audio is to be skipped/removed
            outputarguments.extend(["-map", f"-{inputindex}:a"])
        elif (
            inputaudiocodec == targetaudiocodec
            and inputsamplingrate == targetsamplingrate
            and inputaudiochannels == targetaudiochannels
            and not input.audio == configuration.RemoveOrKeepOrForceRecode.forcerecode
        ):
            # If audio codec and sampling rate is as expected by target
            # and audio is to be forcefully recoded, keep as is
            outputarguments.extend(["-c:a", "copy", "-avoid_negative_ts", "make_zero"])
        elif (
            targetaudiocodec == configuration.Audio.Codec.ogg
            and not oggoutputarguments is None
        ):
            outputarguments.extend(oggoutputarguments)
        elif (
            targetaudiocodec == configuration.Audio.Codec.opus
            and not opusoutputarguments is None
        ):
            outputarguments.extend(opusoutputarguments)
        elif (
            targetaudiocodec == configuration.Audio.Codec.mp3
            and not mp3outputarguments is None
        ):
            outputarguments.extend(mp3outputarguments)
        elif (
            targetaudiocodec == configuration.Audio.Codec.aac
            and not aacoutputarguments is None
        ):
            outputarguments.extend(aacoutputarguments)
        else:
            # Some unsupported audio seems to exist in input file, remove it
            outputarguments.extend(["-map", f"-{inputindex}:a"])

        if input.video == configuration.RemoveOrKeepOrForceRecode.remove:
            # Do not keep video if video is to be skipped/removed
            outputarguments.extend(["-map", f"-{inputindex}:v"])
        elif (
            inputvideocodec == targetvideocodec
            and (inputwidth, inputheight) == size
            and not input.video == configuration.RemoveOrKeepOrForceRecode.forcerecode
        ):
            # If video codec and size is as expected by target
            # and video is to be forcefully recoded, keep as is
            outputarguments.extend(["-c:v", "copy", "-avoid_negative_ts", "make_zero"])
        else:
            if (inputwidth, inputheight) != size:
                outputarguments.extend(
                    [
                        "-vf",
                        f"scale={size[0]}:{size[1]}:force_original_aspect_ratio=decrease,pad={size[0]}:{size[1]}:(ow-iw)/2:(oh-ih)/2",
                    ]
                )
            if targetvideocodec == configuration.Video.Codec.x264:
                outputarguments.extend(
                    _videoCodecToFFmpegArguments(outputformat.video, size)
                )
            elif targetvideocodec == configuration.Video.Codec.x265:
                outputarguments.extend(
                    _videoCodecToFFmpegArguments(outputformat.video, size)
                )
            elif targetvideocodec == configuration.Video.Codec.av1:
                outputarguments.extend(
                    _videoCodecToFFmpegArguments(outputformat.video, size)
                )
            elif targetvideocodec == configuration.Video.Codec.vp8:
                outputarguments.extend(
                    _videoCodecToFFmpegArguments(outputformat.video, size)
                )
            elif targetvideocodec == configuration.Video.Codec.vp9:
                outputarguments.extend(
                    _videoCodecToFFmpegArguments(outputformat.video, size)
                )
            elif targetvideocodec == configuration.Video.Codec.ffv1v3:
                outputarguments.extend(
                    _videoCodecToFFmpegArguments(outputformat.video, size)
                )
            else:
                # Some unsupported video seems to exist in input file, remove it
                outputarguments.extend(["-map", f"-{inputindex}:v"])

        if ffprobe.hasSubtitle(filename.name):
            outputarguments.extend(["-c:s", "copy"])

    if not filename.lang is None and (
        len(filename.multilang) < 1 or filename.lang in filename.multilang
    ):
        # Maintain language information in processed media data
        # but only if only one language is associated with this input
        # or if the primary language is in the 'multilang' set
        outputarguments.extend(
            [
                f"-metadata:s:{inputindex}",
                f"language={configuration.LanguageCode.threeletters(filename.lang)}",
            ]
        )

    return (inputarguments, outputarguments, outputextension)


def _inputKeyByMediaType(
    input: typing.Union[configuration.Input, configuration.Sequence],
    videoBeforeAudio: bool = True,
):
    if isinstance(input, configuration.Sequence):
        seq: configuration.Sequence = typing.cast(configuration.Sequence, input)
        if len(seq.inputs) > 0:
            realinputs = list(
                filter(lambda x: isinstance(x, configuration.Input), seq.inputs)
            )
            if len(realinputs) > 0:
                input = typing.cast(configuration.Input, seq.inputs[0])
            else:
                return 100
        else:
            return 100

    if input.type in [configuration.MediaType.video, configuration.MediaType.slides]:
        return 1 if videoBeforeAudio else 10
    elif input.type in [configuration.MediaType.audio, configuration.MediaType.silence]:
        return 10 if videoBeforeAudio else 1
    elif input.type in [
        configuration.MediaType.subtitle,
        configuration.MediaType.manuscript,
    ]:
        return 30
    else:
        return 100


def _buildMerge(
    merge: configuration.Merge,
    outputformat: configuration.Format,
    languages: typing.List[typing.Optional[configuration.LanguageCode]],
    tempdir: str,
) -> typing.Optional[configuration.Input]:
    # Merge 'sequence' tags with multiple input tags into one input tag replacing the sequence
    for input in filter(lambda x: isinstance(x, configuration.Sequence), merge.inputs):
        newinput = _buildSequence(
            typing.cast(configuration.Sequence, input), outputformat, languages, tempdir
        )
        if newinput is None:
            raise RuntimeError(
                "Could not translate a sequence object to a single input object: "
                + repr(input)
            )
        merge.inputs.remove(input)
        merge.inputs.add(newinput)

    duration: int = _inputDuration(merge, languages)
    if duration == sys.maxsize:
        raise ValueError("Could not compute duration for this merge: " + str(merge))

    # Time stamp in manuscript files may be based on time values in audio recordings.
    # If only a certain range of an audio recording is used (e.g. by specifying a
    # TimeFrame for an Input), the start time of this range (the offset) must be taken
    # into account to compute the correct subtitle time stamps.
    # In this computation, audio recordings, if used, take precendence over video
    # recordings, so sort inputs so that audio recodings come before video recordings
    # by specifying  videoBeforeAudio=False  when invoking  _inputKeyByMediaType.
    offsetforsubtitles: int = 0
    for input in sorted(
        merge.inputs, key=lambda x: _inputKeyByMediaType(x, videoBeforeAudio=False)
    ):
        # In case no output audio sampling rate or no audio channel number is set,
        # try to guess from the first possible input encountered
        if (
            isinstance(input, configuration.Input)
            and isinstance(outputformat, configuration.Format)
            and isinstance(outputformat.audio, configuration.Audio)
            and (
                outputformat.audio.samplingrate is None
                or outputformat.audio.channels is None
            )
            and (
                input.type == configuration.MediaType.video
                or input.type == configuration.MediaType.audio
            )
        ):
            _, samplingrate, channels = ffprobe.audioCodec(
                min(input.filenames.values()).name
            )
            if outputformat.audio.samplingrate is None:
                outputformat.audio.samplingrate = samplingrate
            if outputformat.audio.channels is None:
                outputformat.audio.channels = channels

        _preprocessInput(
            input, outputformat, duration, tempdir, offsetforsubtitles, languages
        )
        if offsetforsubtitles == 0 and typing.cast(configuration.Input, input).type in [
            configuration.MediaType.audio,
            configuration.MediaType.video,
            configuration.MediaType.placeholder,
        ]:
            for attr in filter(
                lambda x: isinstance(x, configuration.TimeFrame),
                typing.cast(configuration.Input, input).attributes,
            ):
                offsetforsubtitles = typing.cast(configuration.TimeFrame, attr).start
                break

    ffmpeginputarguments = []
    ffmpegoutputarguments = []
    finallanguages: typing.Set[configuration.LanguageCode] = set()
    inputindex = 0
    outputextensions: typing.Set[str] = set()
    for input in sorted(
        merge.inputs, key=lambda x: _inputKeyByMediaType(x, videoBeforeAudio=False)
    ):
        if not isinstance(input, configuration.Input):
            continue
        elif input.type == configuration.MediaType.placeholder:
            continue
        elif (input.type == configuration.MediaType.video) and not isinstance(
            outputformat.video, configuration.Video
        ):
            # No point in using a video input if no output codec for videos is defined
            continue
        elif input.type == configuration.MediaType.audio and not isinstance(
            outputformat.audio, configuration.Audio
        ):
            # No point in using an audio input if no output codec for audio data is defined
            continue
        elif (
            input.type == configuration.MediaType.subtitle
            and not isinstance(outputformat.video, configuration.Video)
            and not isinstance(
                outputformat.subtitleformat, configuration.SubtitleFormat
            )
        ):
            # Keep subtitles only if either no ouput format is defined or it contains video encoding information or subtitle information
            continue
        elif (
            input.type == configuration.MediaType.manuscript
            or input.type == configuration.MediaType.slides
            or input.type == configuration.MediaType.image
        ):
            # "manuscript", "slides", or "image" should have been processed into something else in "_preprocessInput"
            # If that did not happen it was on purpose or due to a bug, but those inpt types can still not be used here
            continue

        previouslyProcessedFilename: typing.Set[str] = set()
        for _language in languages:
            filename: typing.Optional[configuration.Filename] = _languageFilename(
                input, [_language, None, configuration.LanguageCode.en]
            )
            if filename is None or filename.name in previouslyProcessedFilename:
                continue
            previouslyProcessedFilename.add(filename.name)
            if not filename.lang is None:
                finallanguages.add(filename.lang)
            finallanguages.update(filter(lambda x: not x is None, filename.multilang))

            i, o, ext = _codecArgumentsForFile(
                filename, input, outputformat, inputindex, duration, skipAudio=False
            )
            if not i is None and not o is None and not ext is None and len(i) > 0:
                ffmpeginputarguments.extend(i)
                ffmpegoutputarguments.extend(o)
                outputextensions.add(ext)
                inputindex += 1

    outputextension: Optional[str] = (
        ".mkv"
        if len(outputextensions) > 1
        else (next(iter(outputextensions)) if len(outputextensions) == 1 else None)
    )
    if outputextension is None:
        if (
            not outputformat.subtitleformat is None
            and outputformat.audio is None
            and outputformat.video is None
        ):
            # In case where subtitles were to be generated, but there is no manuscript
            # for the given input (e.g. because there is silence), generate
            # an empty subtitle file.
            outputextension = configuration.SubtitleFormat.extension(
                outputformat.subtitleformat
            )
            outputfilename: str = configuration.tempFilename(tempdir, outputextension)
            subtitle.writeEmptySubtitleFile(outputfilename, duration)
            resultinput: configuration.Input = configuration.Input()
            resultinput.type = configuration.MediaType.subtitle
            resultinput.alignment = merge.alignment
            resultinput.filenames = {
                None
                if len(finallanguages) == 0
                else finallanguages[0]: configuration.Filename(
                    outputfilename,
                    multilang=[x for x in finallanguages if not x is None],
                )
            }
            return resultinput

    if outputextension is None:
        raise ValueError("Could not determine output extension for given input")

    outputfilename: str = configuration.tempFilename(tempdir, outputextension)
    if not merge.title is None:
        metadatafilename = configuration.tempFilename(tempdir, ".txt")
        with open(metadatafilename, "w") as metadatafile:
            print(";FFMETADATA1", file=metadatafile)
            print("\n[CHAPTER]", file=metadatafile)
            print(
                f"TIMEBASE=1/1000\nSTART=0\nEND={duration}\ntitle={merge.title}",
                file=metadatafile,
            )
        ffmpeginputarguments.extend(["-i", metadatafilename])
        ffmpegoutputarguments.extend(["-map_metadata", str(inputindex)])
        inputindex += 1
    ffmpegarguments: typing.List[str] = _removeDuplicateFFmpegArguments(
        ["ffmpeg"]
        + ffmpeginputarguments
        + ffmpegoutputarguments
        + [
            "-pix_fmt",
            "yuv420p",
            "-avoid_negative_ts",
            "make_zero",
            "-t",
            configuration.timeStampToStr(duration),
        ]
        + [outputfilename]
    )

    print("[ffmpeg._buildMerge] Running:", "'" + "' '".join(ffmpegarguments) + "'")
    completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
        ffmpegarguments, capture_output=True
    )
    if completedprocess is None or completedprocess.returncode != 0:
        if not completedprocess is None:
            print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
            print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
        raise RuntimeError(
            "Running command failed: '" + "' '".join(ffmpegarguments) + "'"
        )
        return None

    resultinput: configuration.Input = configuration.Input()
    resultinput.type = configuration.MediaType.fromFilename(outputfilename)
    resultinput.alignment = merge.alignment
    primarylanguage: typing.Optional[configuration.LanguageCode] = (
        next(iter(finallanguages)) if len(finallanguages) == 1 else None
    )
    resultinput.filenames = (
        {None: configuration.Filename(outputfilename, multilang=list(finallanguages))}
        if primarylanguage is None
        else {
            primarylanguage: configuration.Filename(
                outputfilename, lang=primarylanguage
            )
        }
    )
    return resultinput


def _buildSequence(
    sequence: configuration.Sequence,
    outputformat: configuration.Format,
    languages: typing.List[typing.Optional[configuration.LanguageCode]],
    tempdir: str,
) -> typing.Optional[configuration.Input]:
    # Merge 'merge' tags with multiple input tags into one input tag replacing the merge
    for merge in filter(lambda x: isinstance(x, configuration.Merge), sequence.inputs):
        mergedinput = _buildMerge(
            typing.cast(configuration.Merge, merge), outputformat, languages, tempdir
        )
        if mergedinput is None:
            raise RuntimeError(
                "Could not translate a merge object to a single input object: "
                + repr(input)
            )
        pos = sequence.inputs.index(merge)
        sequence.inputs.remove(merge)
        sequence.inputs.insert(pos, mergedinput)

    for input in sequence.inputs:
        # In case no output audio sampling rate or no audio channel number is set,
        # try to guess from the first possible input encountered
        if (
            isinstance(outputformat, configuration.Format)
            and isinstance(outputformat.audio, configuration.Audio)
            and (
                outputformat.audio.samplingrate is None
                or outputformat.audio.channels is None
            )
            and isinstance(input, configuration.Input)
            and (
                input.type == configuration.MediaType.video
                or input.type == configuration.MediaType.audio
            )
        ):
            _, samplingrate, channels = ffprobe.audioCodec(
                min(input.filenames.values()).name
            )
            if outputformat.audio.samplingrate is None:
                outputformat.audio.samplingrate = samplingrate
            if outputformat.audio.channels is None:
                outputformat.audio.channels = channels

        _preprocessInput(
            input,
            outputformat,
            _inputDuration(input, languages),
            tempdir,
            outputlanguages=languages,
        )

    # Can only merge files of a single type (e.g. video, audio)
    # or audio combined with video
    uniqueTypes: typing.Set[configuration.MediaType] = configuration.uniqueTypes(
        sequence.inputs
    )
    if len(uniqueTypes) != 1 and (
        len(uniqueTypes) != 2
        or not configuration.MediaType.audio in uniqueTypes
        or not configuration.MediaType.video in uniqueTypes
    ):
        raise ValueError(
            "Trying to concatenate a sequence of incompatible media: "
            + ", ".join([str(i) for i in sequence.inputs])
        )
    uniqueType: configuration.MediaType = (
        configuration.MediaType.video
        if configuration.MediaType.audio in uniqueTypes
        and configuration.MediaType.video in uniqueTypes
        else uniqueTypes.pop()
    )
    if uniqueType == configuration.MediaType.placeholder:
        placeholderinput: configuration.Input = configuration.Input()
        placeholderinput.type = configuration.MediaType.placeholder
        placeholderinput.alignment = sequence.alignment
        # newinput.filenames = {}
        placeholderinput.attributes = {
            configuration.TimeFrame(
                duration=sum([_inputDuration(inp, [None]) for inp in sequence.inputs])
            )
        }
        return placeholderinput

    finallanguages: typing.Union[
        typing.List[configuration.LanguageCode], typing.Set[configuration.LanguageCode]
    ] = set()
    outputfilename: str = ""

    listoffilenames: typing.List[
        typing.Tuple[typing.List[configuration.LanguageCode], str]
    ] = []
    for input in sequence.inputs:
        assert isinstance(input, configuration.Input)

        filename: typing.Optional[configuration.Filename] = _languageFilename(
            input, languages + [None, configuration.LanguageCode.en]
        )
        if filename is None:
            continue
        typing.cast(
            typing.Set[typing.Optional[configuration.LanguageCode]], finallanguages
        ).update(set(filename.multilang))

        (
            ffmpeginputarguments,
            ffmpegoutputarguments,
            outputextension,
        ) = _codecArgumentsForFile(filename, input, outputformat, 0, skipAudio=False)
        if outputextension is None or len(ffmpeginputarguments) == 0:
            # If no input arguments are given, this input is probably to be skipped (as decided inside _codecArgumentsForFile)
            continue

        outputfilename = configuration.tempFilename(tempdir, outputextension)
        ffmpegarguments: typing.List[str] = (
            ["ffmpeg"]
            + ffmpeginputarguments
            + ffmpegoutputarguments
            + ["-pix_fmt", "yuv420p", "-avoid_negative_ts", "make_zero", outputfilename]
        )

        print(
            "[ffmpeg._buildSequence] Running:", "'" + "' '".join(ffmpegarguments) + "'"
        )
        completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
            ffmpegarguments, capture_output=True
        )
        if completedprocess is None or completedprocess.returncode != 0:
            if not completedprocess is None:
                print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
                print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
            raise RuntimeError(
                "Running command failed: '" + "' '".join(ffmpegarguments) + "'"
            )
            return None

        listoffilenames.append((filename.multilang, outputfilename))

    finallanguages = configuration.LanguageCode.englishfirstlist(
        typing.cast(typing.Set[configuration.LanguageCode], finallanguages)
    )

    if len(listoffilenames) == 1:
        # There is just one file to "merge", so replace the sequence with
        # a single new input object for this file
        lngs, fn = listoffilenames[0]
        newinput: configuration.Input = configuration.Input()
        newinput.type = configuration.MediaType.fromFilename(fn)
        newinput.alignment = sequence.alignment
        newinput.filenames = {
            None
            if len(lngs) < 1 or len(lngs) > 1
            else lngs[0]: configuration.Filename(
                fn, multilang=[x for x in lngs if not x is None]
            )
        }
        return newinput
    elif len(listoffilenames) > 1:
        # Multiple files need to be merged
        outputextension = configuration.MediaType.extension(
            uniqueType,
            outputformat,
            max(
                ffprobe.countStreams(fn[1], configuration.MediaType.subtitle)
                for fn in listoffilenames
            ),
        )
        outputfilename = configuration.tempFilename(tempdir, outputextension)
        if (
            not mergeMedia(
                [b for a, b in listoffilenames],
                outputfilename,
                [],
                outputformat,
                tempdir,
            )
            is None
        ):
            resultinput: configuration.Input = configuration.Input()
            resultinput.type = configuration.MediaType.fromFilename(outputfilename)
            resultinput.alignment = sequence.alignment
            resultinput.filenames = {
                None
                if len(finallanguages) == 0
                else finallanguages[0]: configuration.Filename(
                    outputfilename,
                    multilang=[x for x in finallanguages if not x is None],
                )
            }
            return resultinput

    return None


def generateMedia(
    container: typing.Union[configuration.Merge, configuration.Sequence],
    outputformat: configuration.Format,
    languages: typing.List[typing.Optional[configuration.LanguageCode]],
    tempdir: str,
) -> typing.Optional[str]:
    newinput: typing.Optional[configuration.Input] = None
    if isinstance(container, configuration.Sequence):
        newinput = _buildSequence(container, outputformat, languages, tempdir)
    elif isinstance(container, configuration.Merge):
        newinput = _buildMerge(container, outputformat, languages, tempdir)

    if isinstance(newinput, configuration.Input):
        lfn = _languageFilename(
            newinput, languages + [None, configuration.LanguageCode.en]
        )
        return None if lfn is None else lfn.name

    return None


def mergeMedia(
    listofmediafilenames: list[str],
    outputfilename: str,
    metadata: typing.List[str],
    outputformat: configuration.Format,
    tempdir: str,
) -> typing.Optional[int]:
    if len(listofmediafilenames) == 0:
        print("Nothing to merge, list is empty")
        return None

    concatfilename = configuration.tempFilename(tempdir, "txt")
    with open(concatfilename, "w") as concatfile:
        print("ffconcat version 1.0", file=concatfile)
        for mediafilename in listofmediafilenames:
            print(f"file '{mediafilename}'", file=concatfile)
        print(f"[ffmpeg.mergeMedia] Joining '{', '.join(listofmediafilenames)}'")

    outputarguments: typing.List[str] = ["-map", "0", "-c", "copy"]

    ffmpegarguments: typing.List[str] = [
        "ffmpeg",
        "-y",
        "-f",
        "concat",
        "-safe",
        "0",
        "-i",
        concatfilename,
        *metadata,
        *outputarguments,
    ]
    ffmpegarguments.append(outputfilename)

    print("[ffmpeg.mergeMedia] Running:", "'" + "' '".join(ffmpegarguments) + "'")
    completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
        ffmpegarguments, capture_output=True
    )

    if completedprocess is None or completedprocess.returncode != 0:
        if not completedprocess is None:
            print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
            print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
        with open(concatfilename, "r") as concatfile:
            print("".join(concatfile.readlines()))
        raise RuntimeError(
            "Running command failed: '" + "' '".join(ffmpegarguments) + "'"
        )
        return None

    return ffprobe.duration(outputfilename)
