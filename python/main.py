# SPDX-FileCopyrightText: 2022 Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: AGPL-3.0-or-later

import tempfile
import typing
import sys
import copy

import configuration
import pdftoimage
import subtitle
import ffprobe
import ffmpeg

if len(sys.argv) <= 1:
    raise RuntimeError("Require at least one argument: XML file")

with tempfile.TemporaryDirectory() as tmpdirname:
    movie = configuration.load(sys.argv[-1], preprocessFiles=True, tempDir=tmpdirname)
    duration: typing.Optional[int] = None
    onlyoutput: typing.Optional[str] = None
    for arg1, arg2 in zip(sys.argv[:-1], sys.argv[1:]):
        if arg1 == "--output":
            onlyoutput = arg2

    # Generating output may modify data structures 'concat' and 'output',
    # e.g. by rewriting, replacing, or removing individual 'input' objects.
    # If several outputs are to be generated, the original concat/input and
    # output data structurse must be restored using deep copies.
    copyofconcat: typing.Optional[typing.List[configuration.Merge]] = (
        copy.deepcopy(movie.concat) if len(movie.outputs) > 1 else None
    )
    copyofoutputs: typing.Optional[typing.Set[configuration.Output]] = (
        copy.deepcopy(movie.outputs) if len(movie.outputs) > 1 else None
    )

    if len(movie.outputs) == 0:
        print("No output found in configuration, nothing to do")
        sys.exit(1)

    firstoutput: bool = True
    for output in movie.outputs:
        for outputfile in output.filenames.values():
            if not onlyoutput is None and outputfile.name != onlyoutput:
                # If the user requested only a single outputfile to be generated,
                # skip all other output targets.
                # However, if the filename in 'onlyoutput' is not specified in the
                # used XML file, every output will be skipped as there will be no match.
                print(
                    f"Skipping output {outputfile.name}, as only {onlyoutput} is requested"
                )
                continue

            if (
                not firstoutput
                and not copyofconcat is None
                and not copyofoutputs is None
            ):
                movie.concat = copy.deepcopy(copyofconcat)
                movie.outputs = copy.deepcopy(copyofoutputs)
            firstoutput = False

            output.format.container = configuration.Format.Container.fromExtension(
                configuration.filenameExtension(outputfile.name)
            )

            print(f"[main] About to create output for file '{outputfile.name}'")
            stepffmpegfiles: typing.List[str] = []
            languages: typing.List[
                typing.Optional[configuration.LanguageCode]
            ] = outputfile.multilang
            if len(languages) == 0:
                languages = [None]

            for step in movie.concat:
                if isinstance(step, configuration.Merge) or isinstance(
                    step, configuration.Sequence
                ):
                    stepfilename = ffmpeg.generateMedia(
                        step, output.format, languages, tmpdirname
                    )
                    if not stepfilename is None:
                        stepffmpegfiles.append(stepfilename)

            if len(stepffmpegfiles) > 0:
                for metadatalanguage in languages:
                    metadata = (
                        ffmpeg.metadataParameters(movie.metadata, metadatalanguage)
                        if not movie.metadata is None
                        else []
                    )
                    if len(metadata) > 0:
                        break

                duration = ffmpeg.mergeMedia(
                    stepffmpegfiles,
                    outputfile.name,
                    metadata,
                    output.format,
                    tmpdirname,
                )
                if duration is None:
                    print(
                        f"[main] Encoding towards file '{outputfile.name}' failed",
                        file=sys.stderr,
                    )
                    break
                else:
                    print(
                        f"[main] Resulting file '{outputfile.name}' has duration {configuration.timeStampToStr(duration)}"
                    )

sys.exit(1 if duration is None else 0)
