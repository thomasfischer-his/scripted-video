# SPDX-FileCopyrightText: 2022 Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: AGPL-3.0-or-later

import typing
import poppler
import PIL.Image
from pysingleton import PyMemoized

import configuration


class PDFRenderer(metaclass=PyMemoized):
    def __init__(self, size: typing.Tuple[int, int]) -> None:
        self.renderer: poppler.PageRenderer = poppler.PageRenderer()
        self.size: typing.Tuple[int, int] = size

    def _pageToImage(
        self, page: poppler.page.Page, res: int, highquality: bool = False
    ) -> PIL.Image.Image:
        self.renderer.set_render_hint(
            poppler.cpp.page_renderer.render_hint.antialiasing,
            highquality,
        )
        self.renderer.set_render_hint(
            poppler.cpp.page_renderer.render_hint.text_antialiasing,
            highquality,
        )
        image = self.renderer.render_page(page, xres=res, yres=res)
        image = PIL.Image.frombytes(
            "RGBA", (image.width, image.height), image.data, "raw", str(image.format)
        )
        return image

    def _determineResolution(self, page: poppler.page.Page) -> int:
        # Standard resolution to determine page's size
        res: int = 512
        # Render selected PDF page into an pixmap image
        image = self._pageToImage(page, res, highquality=False)
        res = int(
            min(
                res * self.size[0] / image.width,
                res * self.size[1] / image.height,
            )
            + 0.5
        )
        return res

    def renderPDFpage(
        self,
        pdfdocument: typing.Union[str, poppler.document.Document],
        pagenumber: int,
        outputfile: str,
    ) -> None:
        # Get page from PDF document
        if isinstance(pdfdocument, str):
            pdfdocument = poppler.load_from_file(pdfdocument)
        if not isinstance(pdfdocument, poppler.document.Document):
            raise RuntimeError("Did not get a PDF document as argument")
        page = pdfdocument.create_page(pagenumber - 1)

        pdfimage = self._pageToImage(
            page, self._determineResolution(page), highquality=True
        )
        background = PIL.Image.new("RGBA", self.size, pdfimage.load()[0, 0])
        if pdfimage.width > background.width or pdfimage.height > background.height:
            x = (pdfimage.width - background.width) // 2
            y = (pdfimage.height - background.height) // 2
            imapdfimagege = pdfimage.crop(
                (x, y, x + background.width, y + background.height)
            )

        xoffset: int = (background.width - pdfimage.width) // 2
        yoffset: int = (background.height - pdfimage.height) // 2
        background.paste(pdfimage, (xoffset, yoffset))

        background.save(outputfile, lossless=True)
