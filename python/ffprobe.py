# SPDX-FileCopyrightText: 2022 Thomas Fischer <thomas.fischer@his.se>
# SPDX-License-Identifier: AGPL-3.0-or-later

import subprocess
import os
import sys
import typing
from functools import cache

import configuration
import subtitle


def duration(filename: str) -> typing.Optional[int]:
    if not os.path.exists(filename):
        raise FileNotFoundError(
            "Cannot determine duration for non-existing file: " + filename
        )

    if configuration.filenameExtension(filename) in configuration.MediaType.extensions(
        configuration.MediaType.image
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.manuscript)
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.slides)
    ):
        # Those files cannot have a duration
        return None
    elif configuration.filenameExtension(
        filename
    ) in configuration.MediaType.extensions(configuration.MediaType.subtitle):
        return subtitle.duration(filename)

    ffprobearguments: typing.List[str] = [
        "ffprobe",
        "-show_entries",
        "format=duration",
        "-of",
        "default=noprint_wrappers=1",
        "-v",
        "quiet",
        filename,
    ]
    print("[ffprobe.duration] Running:", "'" + "' '".join(ffprobearguments) + "'")
    completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
        ffprobearguments, capture_output=True
    )

    if completedprocess is None or completedprocess.returncode != 0:
        if not completedprocess is None:
            print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
            print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
        raise RuntimeError(
            "Running command failed: '" + "' '".join(ffprobearguments) + "'"
        )
        return None

    lines = completedprocess.stdout.decode("UTF-8").splitlines()
    for line in lines:
        if len(line) > 10 and line[:9] == "duration=":
            timestr = line[9:].strip()
            if timestr == "N/A":
                return None
            return int(float(timestr) * 1000.0 + 0.5)
    return None


def videoCodec(
    filename: str,
) -> typing.Tuple[
    typing.Optional[configuration.Video.Codec],
    typing.Optional[int],
    typing.Optional[int],
]:
    """
    Probe a given file for its video data's format and return a tuple consisting of
    - used codec of type configuration.Video.Codec,
    - video's width as an integer, and
    - video's height as an integer.

    If the provided filename refers to an non-existing file, a 'FileNotFoundError' exception will be raised.
    If for any other reason any of the return tuple's values cannot be determined for whatever reason, it will be set to 'None'.

    Only the first video stream will be inspected if the file contains multiple video streams.
    """

    if not os.path.exists(filename):
        raise FileNotFoundError(
            "Cannot determine video codec for non-existing file: " + filename
        )

    if configuration.filenameExtension(filename) in configuration.MediaType.extensions(
        configuration.MediaType.audio
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.manuscript)
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.slides)
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.subtitle)
    ):
        # Those files cannot have video
        return (None, None, None)

    ffprobearguments: typing.List[str] = [
        "ffprobe",
        "-select_streams",
        "v:0",
        "-show_entries",
        "stream=codec_name,width,height",
        "-of",
        "default=noprint_wrappers=1",
        "-v",
        "quiet",
        filename,
    ]
    print(f"[ffprobe.videoCodec] Checking video codec of file {filename}")
    print("[ffprobe.videoCodec] Running:", "'" + "' '".join(ffprobearguments) + "'")
    completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
        ffprobearguments, capture_output=True
    )

    if completedprocess is None or completedprocess.returncode != 0:
        if not completedprocess is None:
            print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
            print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
        raise RuntimeError(
            "Running command failed: '" + "' '".join(ffprobearguments) + "'"
        )
        return (None, None, None)

    lines = completedprocess.stdout.decode("UTF-8").splitlines()
    videocodec: typing.Optional[configuration.Video.Codec] = None
    width: typing.Optional[int] = None
    height: typing.Optional[int] = None
    for line in lines:
        if len(line) > 12 and line[:11] == "codec_name=":
            text = line[11:].strip()
            if text[:3] == "h26":
                text = "x26" + text[3:]
            elif text == "hevc":
                text = "x265"
            videocodec = configuration.Video.Codec[text]
        elif len(line) > 7 and line[:6] == "width=":
            width = int(line[6:].strip())
        elif len(line) > 8 and line[:7] == "height=":
            height = int(line[7:].strip())

    print(
        f"[ffprobe.videoCodec] Result is {'No codec' if videocodec is None else videocodec.name}",
        end="",
    )
    if not videocodec is None:
        print(
            f", {'???' if width is None else width}x{'???' if height is None else height}",
            end="",
        )
    print()
    return (videocodec, width, height)


def audioCodec(
    filename: str,
) -> typing.Tuple[
    typing.Optional[configuration.Audio.Codec],
    typing.Optional[int],
    typing.Optional[int],
]:
    """
    Probe a given file for its audio data's format and return a tuple consisting of
    - used codec of type configuration.Audio.Codec,
    - sampling rate as an integer, and
    - number of channels as an integer (typically 1 or 2).

    If the provided filename refers to an non-existing file, a 'FileNotFoundError' exception will be raised.
    If for any other reason any of the return tuple's values cannot be determined for whatever reason, it will be set to 'None'.

    Only the first audio stream will be inspected if the file contains multiple audio streams.
    """
    if not os.path.exists(filename):
        raise FileNotFoundError(
            "Cannot determine audio codec for non-existing file: " + filename
        )

    if configuration.filenameExtension(filename) in configuration.MediaType.extensions(
        configuration.MediaType.image
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.manuscript)
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.slides)
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.subtitle)
    ):
        # Those files cannot have audio
        return (None, None, None)

    ffprobearguments: typing.List[str] = [
        "ffprobe",
        "-select_streams",
        "a:0",
        "-show_entries",
        "stream=codec_name,sample_rate,channels",
        "-of",
        "default=noprint_wrappers=1",
        "-v",
        "quiet",
        filename,
    ]
    print(f"[ffprobe.audioCodec] Checking audio codec of file {filename}")
    print("[ffprobe.audioCodec] Running:", "'" + "' '".join(ffprobearguments) + "'")
    completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
        ffprobearguments, capture_output=True
    )

    if completedprocess is None or completedprocess.returncode != 0:
        if not completedprocess is None:
            print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
            print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
        raise RuntimeError(
            "Running command failed: '" + "' '".join(ffprobearguments) + "'"
        )
        return (None, None, None)

    audiocodec: typing.Optional[configuration.Audio.Codec] = None
    samplingrate: typing.Optional[int] = None
    channels: typing.Optional[int] = None
    lines = completedprocess.stdout.decode("UTF-8").splitlines()
    for line in lines:
        if len(line) > 12 and line[:11] == "codec_name=":
            codec = line[11:].strip()
            if codec == "vorbis":
                audiocodec = configuration.Audio.Codec.ogg
            elif (
                len(codec) > 4
                and codec[:4] == "pcm_"
                and codec[4:]
                in [
                    "alaw",
                    "f32be",
                    "f32le",
                    "f64be",
                    "f64le",
                    "mulaw",
                    "s16be",
                    "s16le",
                    "s24be",
                    "s24le",
                    "s32be",
                    "s32le",
                    "s8",
                    "u16be",
                    "u16le",
                    "u24be",
                    "u24le",
                    "u32be",
                    "u32le",
                    "u8",
                    "vidc",
                ]
            ):
                audiocodec = configuration.Audio.Codec.wav
            else:
                audiocodec = configuration.Audio.Codec[codec]
        elif len(line) > 13 and line[:12] == "sample_rate=":
            samplingrate = int(line[12:])
        elif len(line) == 10 and line[:9] == "channels=":
            channels = int(line[9:])
    print(
        f"[ffprobe.audioCodec] Result is {'No codec' if audiocodec is None else audiocodec.name}",
        end="",
    )
    if not audiocodec is None:
        print(
            f", {'???' if samplingrate is None else samplingrate}Hz, {'??? channels' if channels is None else str(channels)+(' channel' if channels==1 else ' channels')}",
            end="",
        )
    print()
    return (audiocodec, samplingrate, channels)


def hasSubtitle(filename: str) -> typing.Optional[bool]:
    if not os.path.exists(filename):
        raise FileNotFoundError(
            "Cannot determine subtitle for non-existing file: " + filename
        )
    elif configuration.filenameExtension(
        filename
    ) in configuration.MediaType.extensions(configuration.MediaType.subtitle):
        return True
    elif configuration.filenameExtension(
        filename
    ) in configuration.MediaType.extensions(configuration.MediaType.audio).union(
        configuration.MediaType.extensions(configuration.MediaType.image)
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.manuscript)
    ).union(
        configuration.MediaType.extensions(configuration.MediaType.slides)
    ):
        return False

    return countStreams(filename, configuration.MediaType.subtitle) > 0


def countStreams(filename: str, mediaType: configuration.MediaType) -> int:
    if not os.path.exists(filename):
        raise FileNotFoundError(
            "Cannot determine subtitle for non-existing file: " + filename
        )

    return sum(1 for _ in filter(lambda x: x[1] == mediaType, streams(filename)))


def streams(
    filename: str,
) -> typing.Generator[
    typing.Tuple[
        int,
        typing.Optional[configuration.MediaType],
        typing.Optional[configuration.LanguageCode],
    ],
    None,
    None,
]:
    if not os.path.exists(filename):
        raise FileNotFoundError(
            "Cannot determine streams for non-existing file: " + filename
        )

    ffprobearguments: typing.List[str] = [
        "ffprobe",
        "-show_entries",
        "stream=index,codec_type:stream_tags=language",
        "-of",
        "default=noprint_wrappers=1",
        "-v",
        "quiet",
        filename,
    ]
    print("[ffprobe.streams] Running:", "'" + "' '".join(ffprobearguments) + "'")
    completedprocess: typing.Optional[subprocess.CompletedProcess] = subprocess.run(
        ffprobearguments, capture_output=True
    )

    if completedprocess is None or completedprocess.returncode != 0:
        if not completedprocess is None:
            print(completedprocess.stdout.decode("UTF-8"), file=sys.stderr)
            print(completedprocess.stderr.decode("UTF-8"), file=sys.stderr)
        raise RuntimeError(
            "Running command failed: '" + "' '".join(ffprobearguments) + "'"
        )

    index: typing.Optional[int] = None
    codecType: typing.Optional[configuration.MediaType] = None
    language: typing.Optional[configuration.LanguageCode] = None
    lines = completedprocess.stdout.decode("UTF-8").splitlines()
    for line in lines:
        if len(line) > 6 and line[:6] == "index=":
            if not index is None:
                yield (index, codecType, language)
            index = int(line[6:].strip())
        elif len(line) > 11 and line[:11] == "codec_type=":
            codecType = configuration.MediaType[line[11:].strip()]
        elif len(line) > 13 and line[:13] == "TAG:language=":
            text = line[13:].strip()
            if text == "eng":
                language = configuration.LanguageCode.en
            elif text == "deu":
                language = configuration.LanguageCode.de
            elif text == "swe":
                language = configuration.LanguageCode.sv
            else:
                language = None

    if not index is None:
        yield (index, codecType, language)
